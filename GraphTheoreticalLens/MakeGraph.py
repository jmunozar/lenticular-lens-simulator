import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D #used for3D
from matplotlib import cm #used for 3D and countour data
import operator

import numpy as np
import Utils

#fileNameToLoad ='ReportLensCombinations_55InScreenWithiPhoneXPixelSize.txt'

#fileNameToLoad = 'ReportLensCombinations_PG278Q.txt'
fileNameToLoad ='ReportLensCombinations_PG278QSTRAIGHTPixels.txt'

lensRadiusesPercentagesCombinations = []
substrateThicknesessCombinations = []
screenResolution = 0
pixelSize = 0


########################################## Header values extraction ############################################
def ExtractHeaderValues(fileName):
    fileLines = open(fileName, 'r').readlines()

    global screenResolution
    screenResolutionLine = fileLines[2]
    screenResolutionSTR = screenResolutionLine.split('* Total pixels: ')[1]
    screenResolution = int(screenResolutionSTR)

    global pixelSize
    pixelSizeLine = fileLines[1]
    pixelSizeSTR = pixelSizeLine.split('* Pixel Size: ')[1]
    pixelSize = float(pixelSizeSTR)

    # Lens radius combinations
    lensRadiusesLine = fileLines[5]
    global lensRadiusesPercentagesCombinations
    lensRadiusesPercentagesCombinationsSTR = lensRadiusesLine.split('* Lens Radiuses:         ')[1].split(',')[:-1]
    for i in range(0, len(lensRadiusesPercentagesCombinationsSTR)):
        lensRadiusesPercentagesCombinations.append(float(lensRadiusesPercentagesCombinationsSTR[i]))

    # Substrate thickness combinations
    substrateThicknesessLine = fileLines[4]
    global substrateThicknesessCombinations
    substrateThicknesessSTR = substrateThicknesessLine.split('* Substrate thicknesess: ')[1].split(',')[:-1]
    for i in range(0, len(substrateThicknesessSTR)):
        substrateThicknesessCombinations.append(float(substrateThicknesessSTR[i]))

ExtractHeaderValues(fileNameToLoad)

########################################## Data extraction ######################################################
values = np.loadtxt(fileNameToLoad,
                    dtype={'names': ('LensRadius', 'LensRadiusIncrPercent', 'SubstrateThickness', 'PixelsPerLenticule',
                                      'PixelsUser0', 'PercentPixelsUser0', 'SharedPixelsUser0', 'PercentSharedPixelsUser0',
                                      'PixelsUser1', 'PercentPixelsUser1', 'SharedPixelsUser1', 'PercentSharedPixelsUser1',
                                      'PixelsUser2', 'PercentPixelsUser2', 'SharedPixelsUser2', 'PercentSharedPixelsUser2' ),
                            'formats': (np.float, np.float, np.float, np.int,
                                        np.int, np.float, np.int, np.float,
                                        np.int, np.float, np.int, np.float,
                                        np.int, np.float, np.int, np.float)},
                    comments='*',
                    delimiter=' ',
                    skiprows=14,
                    usecols=range(16),
                    converters = {1: Utils.p2f, 5: Utils.p2f, 7:Utils.p2f, 9:Utils.p2f, 11:Utils.p2f, 13: Utils.p2f, 15: Utils.p2f})

########################################## Data processing and drawing ######################################################

fig1 = plt.figure(1)
plt.suptitle('Theoretical lenticular lens for 55in screen with iPhoneX pixel size. Pixel size: ' + str(pixelSize) + ', substrate thickness: 0.5mm, screen resolution: ' + str(screenResolution))
#plt.suptitle('Theoretical lenticular lens for Asus PG278Q screen, pixel size: ' + str(pixelSize) + ', substrate thickness: 0.5mm, screen resolution: ' + str(screenResolution))
arrayOfValuesToFilter = values['SubstrateThickness']
permittedValuesOfArrayOfValues = [0.5]
pixelsPerLenticuleFilteredBySubsThckness = Utils.FilterBy(values['PixelsPerLenticule'], arrayOfValuesToFilter,  permittedValuesOfArrayOfValues)

startingLensRadiusesToDrawIndex = 0 #index of the lensRadiusesPercentagesCombinations array
maxNumLensRadiusesToDrawIndex = 11 #max interval for the lensRadiusesPercentagesCombinations array

### Graph 1
percentPxUsr0FilteredBySubsThckness = Utils.FilterBy(values['PercentPixelsUser0'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
percentPxUsr1FilteredBySubsThckness = Utils.FilterBy(values['PercentPixelsUser1'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
percentPxUsr2FilteredBySubsThckness = Utils.FilterBy(values['PercentPixelsUser2'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
plt.subplot(311)
Utils.Graph2DSeparatedByRadiusPercentageMINYValues('Min pixel percentage from 3 users (closest to 33% the better)',\
                                                   'Pixels per lenticule', pixelsPerLenticuleFilteredBySubsThckness, \
                                                   'Min % users see', [percentPxUsr0FilteredBySubsThckness, \
                                                                               percentPxUsr1FilteredBySubsThckness, \
                                                                               percentPxUsr2FilteredBySubsThckness], \
                                                   lensRadiusesPercentagesCombinations, startingLensRadiusesToDrawIndex, maxNumLensRadiusesToDrawIndex)


### Graph 2
PxUsr0FilteredBySubsThckness = Utils.FilterBy(values['PixelsUser0'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
PxUsr1FilteredBySubsThckness = Utils.FilterBy(values['PixelsUser1'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
PxUsr2FilteredBySubsThckness = Utils.FilterBy(values['PixelsUser2'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)

sharedPxUsr0FilteredBySubsThckness = Utils.FilterBy(values['SharedPixelsUser0'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
sharedPxUsr1FilteredBySubsThckness = Utils.FilterBy(values['SharedPixelsUser1'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)
sharedPxUsr2FilteredBySubsThckness = Utils.FilterBy(values['SharedPixelsUser2'], arrayOfValuesToFilter, permittedValuesOfArrayOfValues)

uniquePxUsr0FilteredBySubsThckness = []
uniquePxUsr1FilteredBySubsThckness = []
uniquePxUsr2FilteredBySubsThckness = []
for i in range(0, len(PxUsr0FilteredBySubsThckness)):
    uniquePxUsr0FilteredBySubsThckness.append(PxUsr0FilteredBySubsThckness[i] - sharedPxUsr0FilteredBySubsThckness[i])
    uniquePxUsr1FilteredBySubsThckness.append(PxUsr1FilteredBySubsThckness[i] - sharedPxUsr1FilteredBySubsThckness[i])
    uniquePxUsr2FilteredBySubsThckness.append(PxUsr2FilteredBySubsThckness[i] - sharedPxUsr2FilteredBySubsThckness[i])

plt.subplot(312)
Utils.Graph2DSeparatedByRadiusPercentageMINYValues('Minimum unique pixels from 3 users (the higher the better)',\
                                                   'Pixels per lenticule', pixelsPerLenticuleFilteredBySubsThckness, \
                                                   'Min unique pixels users see', [uniquePxUsr0FilteredBySubsThckness, \
                                                                                   uniquePxUsr1FilteredBySubsThckness, \
                                                                                   uniquePxUsr2FilteredBySubsThckness], \
                                                   lensRadiusesPercentagesCombinations, startingLensRadiusesToDrawIndex, maxNumLensRadiusesToDrawIndex)


### Graph 3
plt.subplot(313)
Utils.Graph2DSeparatedByRadiusPercentageMAXYValues('Max ghosting from 3 users (the lower the better)',\
                                                   'Pixels per lenticule', pixelsPerLenticuleFilteredBySubsThckness, \
                                                   'Max ghosting pixels', [sharedPxUsr0FilteredBySubsThckness, \
                                                                               sharedPxUsr1FilteredBySubsThckness, \
                                                                               sharedPxUsr2FilteredBySubsThckness], \
                                                   lensRadiusesPercentagesCombinations, startingLensRadiusesToDrawIndex, maxNumLensRadiusesToDrawIndex)



#### 3D Graphs
minRadiusesToDrawIndex = 1
maxRadiusesToDrawIndex = 5
Utils.GraphPixelsPerLenticuleVsSubstrateThicknessVsGhostingPercentageSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minRadiusesToDrawIndex, maxRadiusesToDrawIndex)


Utils.GraphPixelsPerLenticuleVsSubstrateThicknessVsPercentSharedMinUserSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minRadiusesToDrawIndex, maxRadiusesToDrawIndex)


Utils.GraphPixelsPerLenticuleVsSubstrateThicknessVsFullMinimumPixelsSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minRadiusesToDrawIndex, maxRadiusesToDrawIndex)


fig1.tight_layout()
plt.show()
