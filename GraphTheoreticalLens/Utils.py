import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D #used for3D
from matplotlib import colors as mcolors #For generating colors

def p2f(x):
    return float(x.strip('%'))/100

def GetMinimumFromArray(arr):
    return min(arr)

def GetMaximumFromArray(arr):
    return max(arr)

#from https://matplotlib.org/examples/color/named_colors.html
def GenerateAvailableColorsArray():
    colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
    byHSV = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name) # Sort colors by hue, saturation, value and name.
                   for color, name in colors.items())
    sortedNames = [color for hsv, color in byHSV]
    return list(set(sortedNames))#Takeout repeated values

#graphColors = GenerateAvailableColorsArray()
graphColors = ['red', 'green', 'blue', 'gold', 'darkcyan', 'fuchsia', 'darkgray', 'lawngreen', 'orange', 'peru', 'teal', 'steelblue', 'black', 'maroon', 'brown']


def FilterBy(arrayToBeFiltered, arrayOfValues, permittedValuesOfArrayOfValues):
    filteredArray = []
    for i in range(0, len(arrayToBeFiltered)):
        for j in range(0, len(permittedValuesOfArrayOfValues)):
            if arrayOfValues[i] == permittedValuesOfArrayOfValues[j]:
                filteredArray.append(arrayToBeFiltered[i])
                break
    return filteredArray


################################## 2D ######################################
def Graph2DSeparatedByRadiusPercentageMINYValues(tittle, labelX, axisX, labelY, axesY, lensRadiusesPercentages, minNumLensRadiusesToDraw, numLensRadiusesToDraw):
    plt.title(tittle)
    plt.grid(True)
    #### Create arrays of arrays for each of the X and Y axis ####
    axisXArr = []
    minAxisYArr = []#Array that contains the minimum of the values of axesY array of arrays
    for i in range(len(lensRadiusesPercentages)):
        axisXArr.append([])
        minAxisYArr.append([])

    #### Separate values depending on radius percentage and get minimum from each row of the Y axes ###
    for i in range(0, len(axisX)):
        percentageOfRadiusIndex = i % len(lensRadiusesPercentages)

        valuesRowI = []
        for j in range(0, len(axesY)):
            valuesRowI.append(axesY[j][i])
        minAxisYArr[percentageOfRadiusIndex].append(GetMinimumFromArray(valuesRowI))
        axisXArr[percentageOfRadiusIndex].append(axisX[i])

    #### Draw the data ####
    plt.xlabel(labelX)
    plt.ylabel(labelY)

    for i in range(minNumLensRadiusesToDraw, numLensRadiusesToDraw):#len(lensRadiusesPercentages)):
        selectedColor = graphColors[i%len(graphColors)]
        labelValue = 'Rad incr ' + str((lensRadiusesPercentages[i]-1)*100)+'%'
        plt.plot(axisXArr[i], minAxisYArr[i], color=selectedColor, marker='.', label=labelValue)
        plt.legend()

def Graph2DSeparatedByRadiusPercentageMAXYValues(tittle, labelX, axisX, labelY, axesY, lensRadiusesPercentages, minNumLensRadiusesToDraw, numLensRadiusesToDraw):
    plt.title(tittle)
    plt.grid(True)
    #### Create arrays of arrays for each of the X and Y axis ####
    axisXArr = []
    minAxisYArr = []#Array that contains the minimum of the values of axesY array of arrays
    for i in range(len(lensRadiusesPercentages)):
        axisXArr.append([])
        minAxisYArr.append([])

    #### Separate values depending on radius percentage and get minimum from each row of the Y axes ###
    for i in range(0, len(axisX)):
        percentageOfRadiusIndex = i % len(lensRadiusesPercentages)

        valuesRowI = []
        for j in range(0, len(axesY)):
            valuesRowI.append(axesY[j][i])
        minAxisYArr[percentageOfRadiusIndex].append(GetMaximumFromArray(valuesRowI))
        axisXArr[percentageOfRadiusIndex].append(axisX[i])

    #### Draw the data ####
    plt.xlabel(labelX)
    plt.ylabel(labelY)

    for i in range(minNumLensRadiusesToDraw, numLensRadiusesToDraw):#len(lensRadiusesPercentages)):
        selectedColor = graphColors[i%len(graphColors)]
        labelValue = 'Rad incr ' + str((lensRadiusesPercentages[i]-1)*100)+'%'
        plt.plot(axisXArr[i], minAxisYArr[i], color=selectedColor, marker='.', label=labelValue)
        plt.legend()


################################# 3D ######################################
######## Pixels Per Lenticule VS Substrate Thickness VS Ghosting ##########
def GraphPixelsPerLenticuleVsSubstrateThicknessVsGhostingPercentageSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):
    #### Create array of arrays for each of the values ####
    pixelsPerLenticuleSeparatedByRadiusPercentage = []
    substrateThicknessSeparatedByRadiusPercentage = []
    maxGhostingValuesSeparatedByRadiusPercentage = []
    for i in range(len(lensRadiusesPercentagesCombinations)):
        maxGhostingValuesSeparatedByRadiusPercentage.append([])
        pixelsPerLenticuleSeparatedByRadiusPercentage.append([])
        substrateThicknessSeparatedByRadiusPercentage.append([])

    #### Separate values depending on their radius percentage ####
    for i in range(0,len(values)):#for i to n
        percentageOfRadiusIndex = i % len(lensRadiusesPercentagesCombinations)

        ghostingValuesUsers = [values['PercentSharedPixelsUser0'][i], values['PercentSharedPixelsUser1'][i], values['PercentSharedPixelsUser2'][i]]
        maxGhostingValuesSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(max(ghostingValuesUsers))

        pixelsPerLenticuleSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['PixelsPerLenticule'][i])
        substrateThicknessSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['SubstrateThickness'][i])

    ### Draw the 3D data ###
    fig = plt.figure()
    ax = Axes3D(fig)
    plt.suptitle('Max ghosting from 3 users (the lower the better)')
    for i in range(minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):#len(lensRadiusesPercentages)):#Interesting that r is the one that minimizes ghosting.
        x = pixelsPerLenticuleSeparatedByRadiusPercentage[i]
        y = substrateThicknessSeparatedByRadiusPercentage[i]
        z = maxGhostingValuesSeparatedByRadiusPercentage[i]
        labelValue = 'Rad incr ' + str((lensRadiusesPercentagesCombinations[i]-1)*100) + '%'

        selectedColor = graphColors[i%len(graphColors)]
        ax.scatter(x, y, z, c=selectedColor, marker='.', label=labelValue)
        #ax.plot_trisurf(x, y, z, color=graphColors[i%len(graphColors)],linewidth=0.1)
        plt.legend()
    ax.set_xlabel('Pixels per lenticule')
    ax.set_ylabel('Substrate thickness')
    ax.set_zlabel('%Ghosting')

    ######### Pixels per lenticule vs substrate thickness vs the minimum percentage of pixels a user sees ##########
def GraphPixelsPerLenticuleVsSubstrateThicknessVsPercentSharedMinUserSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):
    #### Create array of arrays for each of the values ####
    pixelsPerLenticuleSeparatedByRadiusPercentage = []
    substrateThicknessSeparatedByRadiusPercentage = []
    minPixelPerUserPercentValuesSeparatedByRadiusPercentage = []
    for i in range(len(lensRadiusesPercentagesCombinations)):
        minPixelPerUserPercentValuesSeparatedByRadiusPercentage.append([])
        pixelsPerLenticuleSeparatedByRadiusPercentage.append([])
        substrateThicknessSeparatedByRadiusPercentage.append([])

    #### Separate values depending on their radius percentage ####
    for i in range(0,len(values)):#for i to n
        percentageOfRadiusIndex = i % len(lensRadiusesPercentagesCombinations)

        pixelPercentValuesUsers = [values['PercentPixelsUser0'][i], values['PercentPixelsUser1'][i], values['PercentPixelsUser2'][i]]
        minPixelPerUserPercentValuesSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(min(pixelPercentValuesUsers))

        pixelsPerLenticuleSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['PixelsPerLenticule'][i])
        substrateThicknessSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['SubstrateThickness'][i])

    ### Draw the 3D data ###
    fig = plt.figure()
    ax = Axes3D(fig)
    plt.suptitle('Min pixel percentage from 3 users (closest to  33% better)')
    for i in range(minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):#len(lensRadiusesPercentagesCombinations)):
        x = pixelsPerLenticuleSeparatedByRadiusPercentage[i]
        y = substrateThicknessSeparatedByRadiusPercentage[i]
        z = minPixelPerUserPercentValuesSeparatedByRadiusPercentage[i]
        labelValue = 'Rad incr ' + str((lensRadiusesPercentagesCombinations[i]-1)*100) + '%'
        ax.scatter(x, y, z, c=graphColors[i%len(graphColors)], marker='.', label=labelValue)
        #ax.plot_trisurf(x, y, z, color=graphColors[i%len(graphColors)],linewidth=0.1)
        plt.legend()
    ax.set_xlabel('Pixels per lenticule')
    ax.set_ylabel('Substrate thickness')
    ax.set_zlabel('min % users see')

######### Pixels per lenticule vs substrate thickness vs the minimum separate (not shared) number of pixels users see ##########
def GraphPixelsPerLenticuleVsSubstrateThicknessVsFullMinimumPixelsSeparatedByRadiusPercentage(lensRadiusesPercentagesCombinations, values, minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):
    #### Create array of arrays for each of the values ####
    pixelsPerLenticuleSeparatedByRadiusPercentage = []
    substrateThicknessSeparatedByRadiusPercentage = []
    minUniquePixelsUsersPerceiveSeparatedByRadiusPercentage = []

    for i in range(len(lensRadiusesPercentagesCombinations)):
        minUniquePixelsUsersPerceiveSeparatedByRadiusPercentage.append([])
        pixelsPerLenticuleSeparatedByRadiusPercentage.append([])
        substrateThicknessSeparatedByRadiusPercentage.append([])

    #### Separate values depending on their radius percentage ####
    for i in range(0,len(values)):#for i to n
        percentageOfRadiusIndex = i % len(lensRadiusesPercentagesCombinations)

        uniquePixelsForUsers = [values['PixelsUser0'][i] - values['SharedPixelsUser0'][i], values['PixelsUser1'][i] - values['SharedPixelsUser1'][i], values['PixelsUser2'][i] - values['SharedPixelsUser2'][i]]
        minUniquePixelsUsersPerceiveSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(min(uniquePixelsForUsers))

        pixelsPerLenticuleSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['PixelsPerLenticule'][i])
        substrateThicknessSeparatedByRadiusPercentage[percentageOfRadiusIndex].append(values['SubstrateThickness'][i])

    ### Draw the 3D data ###
    fig = plt.figure()
    ax = Axes3D(fig)
    plt.suptitle('Min unique pixels from 3 users (Higher the better)')
    for i in range(minNumLensRadiusesToDrawIndex, numLensRadiusesToDraw):#len(lensRadiusesPercentagesCombinations)):
        x = pixelsPerLenticuleSeparatedByRadiusPercentage[i]
        y = substrateThicknessSeparatedByRadiusPercentage[i]
        z = minUniquePixelsUsersPerceiveSeparatedByRadiusPercentage[i]
        labelValue = 'Rad incr ' + str((lensRadiusesPercentagesCombinations[i]-1)*100) + '%'
        ax.scatter(x, y, z, c=graphColors[i%len(graphColors)], marker='.', label=labelValue)
        #ax.plot_trisurf(x, y, z, color=graphColors[i%len(graphColors)],linewidth=0.1)
        #ax.contourf(x, y, z, color=graphColors[i%len(graphColors)],linewidth=0.1)
        plt.legend()
    ax.set_xlabel('Pixels per lenticule')
    ax.set_ylabel('Substrate thickness')
    ax.set_zlabel('min unique pixels users see from screen')
