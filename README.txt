This is an idea for a PhD Thesis; doing multiuser 3D trough lenticular
lenses.

A lenticular lens is basically an array of magnifiying lenses desugbed si
tgat when viewed from slightly different angles, different images are
magnified, the idea of this thesis is to be able to have different users
watch the same scene but with depthness crafted for each user's position and
rotation.

A more detailed explanation on that a lenticular lens is can be found at:
https://en.wikipedia.org/wiki/Lenticular_lens

Organization:

LenticularLensSimulator -> Lenticular lens simulator
                        This is a simulator that was developed in order to
                        understand better how lenticular lenses refract rays
                        of pixels depending on de pitch, substrate thickness
                        and other factors that vary in a lenticular
                        lens. This simulator was developed in C++ using
                        glm. For more info on the requirements to compile the
                        simulator please follow the README.txt at
                        LenticularLensSimulator/README.txt inside the
                        simulator folder

GraphTheoreticalLens ->	This is a simple python script that graphs the data generated
                     	by the lenticular lens simulator combination script in order
                        to assess lens values.

LenticularLensOptiXRaytracer -> This is a raytracer that simulates an
                                image under a lenticular lens; with
                                this one can see color binding from
                                lenses. Developed in C++, check
                                README.txt at
                                LenticularLensOptixRaytracer/README.txt
                                for more details.
