#include "Line.h"

using namespace::std;

Line::Line(const glm::vec2 &p1, const glm::vec2 &p2) {
    //std::cout << "p1: " << p1.x << ", " << p1.y << "  p2: " << p2.x << ", " << p2.y << std::endl;
    if(p1.x == p2.x) {//special case, infinite slope (vertical line)
        infiniteSlope = true;
        functionIfInfiniteSlope = p1.x;
        m = b = std::numeric_limits<float>::max();
        //cout << "INFINITE SLOPE" << endl;
    } else {
        m = (p2.y-p1.y) / (p2.x-p1.x);
    }
    b = p1.y - (m*p1.x);
}

Line::Line(float _m, float _b) : infiniteSlope {false}, m {_m}, b {_b} {}

float Line::GetFunctionIfInfiniteSlope() const {
    if(!infiniteSlope) {
        cerr << "ERROR: Line doesnt have an infinite slope" << endl;
        return std::numeric_limits<float>::max();
    } else
        return functionIfInfiniteSlope;
}

float Line::GetM() const {
    if(infiniteSlope) {
        cerr << "ERROR: Slope is infinite, use FunctionIfInfiniteslope instead." << endl;
        return std::numeric_limits<float>::max();
    } else {
        return m;
    }
}

float Line::GetB() const {
    if(infiniteSlope) {
        cerr << "ERROR: Slope is infinite, use FunctionIfInfiniteslope instead" << endl;
        return std::numeric_limits<float>::max();
    } else {
        return b;
    }
}
