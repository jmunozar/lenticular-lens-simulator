#pragma once

#include <iostream>
#include <memory>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "User.h"
#include "Utils.h"
#include "Screen.h"
/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This class is in charge of adding / removing users, positioning them
  automatically and rendering them
 */

class UserManager {
 private:
    std::vector<std::unique_ptr<User>> users;

    Camera &camera;
    GLuint shaderProgramId;
    Screen &screen;//ask carsten why this has to be a ref

    int selectedUserIndex = -1;//used when selecting users for moving them
 public:
    UserManager(Camera &cam, GLuint _shaderProgamId,  Screen &screen);
    int GetUserCount() { return users.size(); }
    void RenderUsers();
    void AddUser(float xPos = 0);
    void RemoveUser();
    void ClearUsers();
    string HighlightPixelsForEachUser();


    User& GetUser(int index) { return *users[index]; }
    void SelectClosestUser(const glm::vec3 &worldPos);
    void MoveClosestUser(const glm::vec3 &worldPos);
    void ClearSelectedUser() { selectedUserIndex = -1; }
};
