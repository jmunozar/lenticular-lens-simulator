#include "Lens.h"

using namespace::std;

Lens::Lens(Camera &renderingCamera, const glm::vec3 &pos, const glm::vec4 &color,
           float radius, float lensAngle, GLuint _shaderProgramId)
    : camera {renderingCamera}, modelMatrix {glm::mat4(1)}, position {pos}, shaderProgramId {_shaderProgramId} {

        lensColorArr[0] = color.r;
        lensColorArr[1] = color.b;
        lensColorArr[2] = color.g;
        lensColorArr[3] = color.a;

        GenerateVertices(radius, lensAngle);
        LoadShaderValues();
        InitializeLensForRendering();
}

Lens::~Lens() {
    DestroyLens();
}

void Lens::GenerateVertices(float lensRadius, float lensAngle) {
    int verticesPerLens = 25;//vertices a whole semicircle will contain
    //lensAngle is the angle the lens has from one lens to the other (the curvature of the pitch)
    double step = lensAngle/((float)verticesPerLens);
    double startingAngle = (180.0-lensAngle)/2;

    for(int i = verticesPerLens; i >= 0; i--) {//draws from left to right
        float x = lensRadius * cos(Utils::Deg2Rad(startingAngle + step*(double)i));
        float y = lensRadius * sin(Utils::Deg2Rad(startingAngle + step*(double)i));
        vertices.push_back(position + glm::vec3(x,y,0));
    }

}

void Lens::LoadShaderValues() {
    projectionHandle = glGetUniformLocation(shaderProgramId, "projectionMatrix");
    viewHandle = glGetUniformLocation(shaderProgramId, "viewMatrix");
    modelHandle = glGetUniformLocation(shaderProgramId, "modelMatrix");
    colorHandle = glGetUniformLocation(shaderProgramId, "color");

    Utils::ExitOnGLError("Couldnt get shader uniforms for Model view and projection matrices for lens");
}

void Lens::InitializeLensForRendering() {
    glGenVertexArrays(1, &lensVAO);
    glBindVertexArray(lensVAO);
    //vertices
    glGenBuffers(1, &verticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    Utils::ExitOnGLError("ERROR: Couldn't create Vertex Array Object for lens.");
}

void Lens::Render() {
    glUseProgram(shaderProgramId);

    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
    glProgramUniform4fv(shaderProgramId, colorHandle, 1, lensColorArr);

    glBindVertexArray(lensVAO);
    glDrawArrays(GL_LINE_STRIP, 0, vertices.size());
    Utils::ExitOnGLError("Error: couldnt draw lens.");
    glBindVertexArray(0);
}

void Lens::DestroyLens() {
    //cout << "Destroying lens" << endl;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &verticesBufferHandle);
    Utils::ExitOnGLError("Couldnt destroy lens vertex array");

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &lensVAO);
    Utils::ExitOnGLError("Couldnt destroy lens VAO");
}
