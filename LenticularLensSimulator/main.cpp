#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <memory>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Utils.h"
#include "Camera.h"
#include "MenuApp.h"
#include "LenticularSheet.h"
#include "Screen.h"
#include "ConfigStructures.h"
#include "Debug.h"

#include "UserManager.h"

using namespace::std;

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 800


GLFWwindow* window;
int frameCount = 0;
string windowTitleStr = "Lenticular lens simulator";

//shader program handles
GLuint flatShaderProgramIdHandle;

void Initialize(void);
void InitWindow(void);
void Timer();
void LoadConfigValues(LenticularConfig &, ScreenConfig &);
void LoadShaders();
void Cleanup();


void ErrorCallback(int error, const char* description) {
    cerr << "ERROR: " << description << endl;
}

void Cleanup() {
    cout << "Cleaning out shaders and VBOs." << endl;
    cout << " |-> Cleaning flat shader program" << endl;
    glDeleteProgram(flatShaderProgramIdHandle);
    Utils::ExitOnGLError("Couldnt delete flat shader program Id");
}


int main(int argc, char* argv[]) {
    Initialize();//called before anything!!!
    int width = WINDOW_WIDTH;
    int height = WINDOW_HEIGHT;

    //these are just structures that get filled in order to pass them as
    //parameters to the screen and lenticular sheet objs
    LenticularConfig lenticularCfg;
    ScreenConfig screenCfg;
    LoadConfigValues(lenticularCfg, screenCfg);

    LoadShaders();
    Camera camera(window);

    Debug::GetInstance().Initialize(camera, flatShaderProgramIdHandle, 1);//initialize graphical debugger


    glm::vec3 position = glm::vec3(0,0,0);
    LenticularSheet lenticularSheet(camera, position, glm::vec4(1,1,1,1), flatShaderProgramIdHandle, lenticularCfg);
    Screen screen(camera, position, flatShaderProgramIdHandle, screenCfg, lenticularSheet);
    bool keyPressed = false;
    UserManager userMgr(camera, flatShaderProgramIdHandle, screen);//ask
                                                                   //carsten
                                                                   //why
                                                                   //screen
                                                                   //has to
                                                                   //be a ref
    MenuApp menu(window, camera, userMgr, lenticularCfg, screen, lenticularSheet);//all params are references here
    {
        /*User c(camera, flatShaderProgramIdHandle, glm::vec3(100,100,0),
               glm::vec4(1, 0.87843, 0.7412,1));//skin color
        */
        //RENDER LOOP
        while (!glfwWindowShouldClose(window)) {
            glfwGetFramebufferSize(window, &width, &height);
            glViewport(0, 0, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glfwPollEvents();
            menu.SetupNewFrame();


            if(glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
                if(!keyPressed) {//hack to get 1 key press
                    cout << "Reloading values from config file." << endl;
                    LoadConfigValues(lenticularCfg, screenCfg);
                    menu.ReloadMenuValues();
                    lenticularSheet.Initialize();
                    screen.Initialize();
                    keyPressed = true;
                }
            } else {
                keyPressed = false;
            }

            camera.GetInput(ImGui::GetIO().MouseWheel);

            if(menu.NeedToUpdateSimulation()) {
                menu.ReloadMenuValues();
                lenticularSheet.Initialize();
                screen.Initialize();
            }
            //bool testWindow = true;
            //ImGui::ShowTestWindow(&testWindow);


            //Render here
            menu.Render();
            userMgr.RenderUsers();
            lenticularSheet.Render();
            screen.Render();

            //graphical debugging, remove when finished
            Debug::GetInstance().RenderDebugInfo();


            Timer();
            ImGui::Render();
            glfwSwapBuffers(window);
        }

    }
    //cleanup
    Cleanup();
    menu.Cleanup();
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

void LoadConfigValues(LenticularConfig &lenticularCfg, ScreenConfig &screenCfg) {
    if(!Utils::LoadConfigFile("Config.txt", lenticularCfg, screenCfg)) {
        cerr << "Config file couldnt be read correctly, aborting." << endl;
        exit(EXIT_FAILURE);
    }
    if(lenticularCfg.lenticularPitch > lenticularCfg.lenticularRadius*2.0) {
        cerr << "Lenticular Pitch cannot be bigger than diameter.\n" <<
            "Pitch: " << lenticularCfg.lenticularPitch << " Radius: " << lenticularCfg.lenticularRadius << endl;
        exit(EXIT_FAILURE);
    }
}

void Initialize() {
    InitWindow();
    GLenum glewInitResult;
    glewExperimental = GL_TRUE;
    glewInitResult = glewInit();
    if (GLEW_OK != glewInitResult) {
        cerr <<  "ERROR: " << glewGetErrorString(glewInitResult) << endl;
        exit(EXIT_FAILURE);
    }
    cout << "INFO: OpenGL Context " << glGetString(GL_VERSION) << endl;

    glClearColor(0,0,0.4f,1);//dark blue bg

    glDisable(GL_CULL_FACE);//disabled when using transparency
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    Utils::ExitOnGLError("ERROR: Couldnt set OpenGL culling options");

    //bleding is set inside Pixel.cpp
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);

}

void InitWindow() {
    glfwSetErrorCallback(ErrorCallback);
    if (!glfwInit())
        exit(EXIT_FAILURE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_DECORATED, GL_FALSE);

    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, windowTitleStr.c_str(), NULL, NULL);

    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void LoadShaders() {
    flatShaderProgramIdHandle = Utils::LoadAndLinkShadersToProgram("FlatShader.vertex.glsl",
                                                                   "FlatShader.fragment.glsl");
    Utils::ExitOnGLError("Couldnt load flat shaders");
}

double lastTimeCall = 0;
void Timer() {
    // Measure speed
    double currentTime = glfwGetTime();
    frameCount++;
    if (currentTime - lastTimeCall >= 1.0){ // If last cout was more than 1 sec ago
        windowTitleStr = to_string(1000.0/double(frameCount)) + "ms FPS: " + to_string(frameCount);
        glfwSetWindowTitle(window, windowTitleStr.c_str());
        //cout << 1000.0/double(frameCount) << "ms FPS:" << frameCount << endl;
        frameCount = 0;
        lastTimeCall += 1.0;
    }
}

