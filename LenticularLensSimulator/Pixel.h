#pragma once

#include <GL/glew.h>

#include <iostream>
#include <array>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"
#include "LineStrip.h"
#include "Utils.h"
#include "ConfigStructures.h"
#include "LenticularSheet.h"
#include "Solver.h"

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.comment

  Pixel representation and rendering; a pixel contains rays
 */
class Pixel {
 private:
    Camera& camera;//<--rendering camera
    glm::mat4 modelMatrix;

    float pixelColorArr[4];
    float redSubPixelColorArr[4]  { 1, 0, 0, 1 };//subpixels use the alpha from the ctor
    float greenSubPixelColorArr[4]{ 0, 1, 0, 1 };
    float blueSubPixelColorArr[4] { 0, 0, 1, 1 };

    glm::vec3 position;
    float width;
    float height;

    std::array<glm::vec3, 4> verticesPixelBackground;
    std::array<glm::vec3, 4> verticesRedSubPixel;
    std::array<glm::vec3, 4> verticesGreenSubPixel;
    std::array<glm::vec3, 4> verticesBlueSubPixel;

    int raysPerPixel;
    std::vector<std::unique_ptr<LineStrip>> rays;

    bool renderRays = true;

    //glsl handles
    GLuint projectionHandle;
    GLuint viewHandle;
    GLuint modelHandle;
    GLuint colorHandle;

    GLuint shaderProgramId;

    GLuint pixelBackgroundVAO;
    GLuint redSubPixelVAO;
    GLuint greenSubPixelVAO;
    GLuint blueSubPixelVAO;

    GLuint verticesBackgroundBufferHandle;
    GLuint redSubPixelVerticesBufferHandle;
    GLuint greenSubPixelVerticesBufferHandle;
    GLuint blueSubPixelVerticesBufferHandle;

    void LoadShaderValues();
    void InitializePixelForRendering();

    void GeneratePixelVertices();
    void DestroyPixel();
    void GenerateRaysForPixel(const LenticularConfig &lensCfg, const ScreenConfig &screenConfig,
                              const LenticularSheet &lensSheet, const glm::vec4 &raysColor);
 public:
    Pixel(Camera &renderingCamera, const glm::vec3 &position, float width, float height, int _raysPerPixel,
          const LenticularConfig &lensConfig, const ScreenConfig &screenConfig,
          const LenticularSheet &lensSheet, GLuint pixelShaderProgramId,
          const glm::vec4 &color = glm::vec4(0,0,0,1));
    Pixel(Pixel const& source) = delete;//means this class cannot be copy constructed
    Pixel& operator=(Pixel const& source) = delete;//means this class cannot be copy assigned
    ~Pixel();

    void SetPixelColor(const glm::vec4 &newColor);
    const std::vector<std::unique_ptr<LineStrip>>& GetPixelRays() { return rays; }

    void Render();
    void RenderPixelRays(bool val) { renderRays = val; }
};
