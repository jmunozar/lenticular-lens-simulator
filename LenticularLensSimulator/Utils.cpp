#include "Utils.h"

void Utils::ExitOnGLError(const string &errorMsg) {
    const GLenum errorValue = glGetError();
    if(errorValue != GL_NO_ERROR) {
        cerr << "ERROR: " << errorMsg << endl;
        exit(EXIT_FAILURE);
    }
}

glm::vec4 Utils::GenerateRandomColor(float alpha) {
    float r = ((double) rand() / (RAND_MAX));
    float g = ((double) rand() / (RAND_MAX));
    float b = ((double) rand() / (RAND_MAX));
    return glm::vec4(r, g, b, alpha);
}

//returns a program ID
GLuint Utils::LoadAndLinkShadersToProgram(const string &vertexShaderFileName, const string &fragmentShaderFileName) {
    GLuint vertexShaderID = LoadSpecificShader(vertexShaderFileName, GL_VERTEX_SHADER);
    GLuint fragmentShaderID = LoadSpecificShader(fragmentShaderFileName, GL_FRAGMENT_SHADER);
    glGetError();
    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertexShaderID);
    glAttachShader(programID, fragmentShaderID);
    glLinkProgram(programID);
    ExitOnGLError("Couldn't link program");

    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);
    return programID;
}


//returns the specific shader Id
GLuint Utils::LoadSpecificShader(const string &fileName, GLenum shaderType) {
    glGetError();

    GLuint shaderId = 0;
    string shaderSourceStr = "";

    ifstream shaderFile(fileName);

    if(shaderFile.is_open()) {
        string line = "";
        while(getline(shaderFile, line))
            shaderSourceStr += line + "\n";
        shaderFile.close();
    } else {
        cerr << "ERROR: Couldnt open file: " << fileName << endl;
    }
    shaderId = glCreateShader(shaderType);
    if(shaderId == 0)
        cerr << "ERROR: Couldnt create shader" << endl;

    vector<char> writable(shaderSourceStr.begin(), shaderSourceStr.end());
    writable.push_back('\0');
    char* c_string = &writable[0];

    cout << "Compiling " << fileName << "..." << endl;

    glShaderSource(shaderId, 1, &c_string, NULL);
    glCompileShader(shaderId);
    GLint result = GL_FALSE;
    int infoLogLength;
    // Check shader
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
    if(infoLogLength > 0 ){
        std::vector<char> shaderErrorMessage(infoLogLength+1);
        glGetShaderInfoLog(shaderId, infoLogLength, NULL, &shaderErrorMessage[0]);
        string msg(shaderErrorMessage.begin(), shaderErrorMessage.end());
        cerr << msg << endl;
        exit(EXIT_FAILURE);
    }
    ExitOnGLError("Couldnt compile shader");
    return shaderId;
}



#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII
//returns the handle in OpenGL for the texture.
GLuint Utils::LoadDDS(const string &imagePath) {
    unsigned char header[124];
    FILE *fp;
    /* try to open the file */
    fp = fopen(imagePath.c_str(), "rb");
    if (fp == NULL){
        cerr << imagePath << " coulnt be opened. Are you in the right directory." << endl;
        exit(EXIT_FAILURE);
    }
    /* verify the type of file */
    char filecode[4];
    fread(filecode, 1, 4, fp);

    if (strncmp(filecode, "DDS ", 4) != 0) {//ERROR ACA
        fclose(fp);
        exit(EXIT_FAILURE);
    }

    /* get the surface desc */
    fread(&header, 124, 1, fp);
    unsigned int height      = *(unsigned int*)&(header[8 ]);
    unsigned int width       = *(unsigned int*)&(header[12]);
    unsigned int linearSize  = *(unsigned int*)&(header[16]);
    unsigned int mipMapCount = *(unsigned int*)&(header[24]);
    unsigned int fourCC      = *(unsigned int*)&(header[80]);

    unsigned char * buffer;
    unsigned int bufsize;
    /* how big is it going to be including all mipmaps? */
    bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize;
    buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
    fread(buffer, 1, bufsize, fp);
    /* close the file pointer */
    fclose(fp);

    //unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4;
    unsigned int format;
    switch(fourCC) {
    case FOURCC_DXT1:
        format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
        break;
    case FOURCC_DXT3:
        format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
        break;
    case FOURCC_DXT5:
        format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
        break;
    default:
        free(buffer);
        return 0;
    }
    // Create one OpenGL texture
    GLuint textureID;
    glGenTextures(1, &textureID);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, textureID);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
    unsigned int offset = 0;

    /* load the mipmaps */
    for (unsigned int level = 0; level < mipMapCount && (width || height); ++level) {
        unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize;
        glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,
                               0, size, buffer + offset);
        offset += size;
        width  /= 2;
        height /= 2;

        // Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
        if(width < 1) width = 1;
        if(height < 1) height = 1;
    }
    free(buffer);
    return textureID;
}

//TODO: This can be architectured better
bool Utils::LoadConfigFile(const string &path, LenticularConfig &refLensConfig, ScreenConfig &refScreenConfig) {
    ifstream fin(path);
    string line;
    int lineNumber = 1;
    bool parseWasSuccess = true;
    while (getline(fin, line)) {
        if(line[0] != '#' && line != "") {
            if(line.find("=") != -1) {//if string contains "="
                string valueStr = line.substr(line.find("=") + 1);
                Trim(valueStr);
                if(StringIsFloat(valueStr)) {//TODO: change this as some values are just integers
                    //-- Lenticular specific configuration values --//
                    if (line.find("LenticularPitch") != -1) {
                        refLensConfig.lenticularPitch = stof(valueStr);
                    } else if (line.find("LensesPerSheet") != -1) {
                        refLensConfig.lensesPerSheet = stof(valueStr);
                    }else if (line.find("LenticularRadius") != -1) {
                        refLensConfig.lenticularRadius = stof(valueStr);
                    } else //if (line.find("LenticularThickness") != -1) {
                        //                        refLensConfig.lenticularThickness = stof(valueStr);
                        /*} else*/ if (line.find("LenticularSubstrateThickness") != -1) {
                        refLensConfig.lenticularSubstrateThickness = stof(valueStr);
                    } else if (line.find("LenticularLensRefraction") != -1) {
                        refLensConfig.lenticularLensRefraction = stof(valueStr);
                    }
                    //-- Screen specific configuration values --//
                    else if(line.find("NumberOfPixelsInScreen") != -1) {
                        refScreenConfig.numberOfPixels = stoi(valueStr);
                    } else if(line.find("SpaceBetweenPixels") != -1) {
                        refScreenConfig.spaceBetweenPixels = stof(valueStr);
                    } else if(line.find("PixelWidth") != -1) {
                        refScreenConfig.pixelWidth = stof(valueStr);
                    } else if(line.find("PixelHeight") != -1) {
                        refScreenConfig.pixelHeight = stof(valueStr);
                    } else if(line.find("RaysPerPixel") != -1) {
                        refScreenConfig.raysPerPixel = stoi(valueStr);
                    } else if(line.find("RaysTransparency") != -1) {
                        refScreenConfig.raysTransparency = stof(valueStr);
                    } else if(line.find("UserDistanceFromScreen") != -1) {
                        refScreenConfig.userDistanceFromScreen = stof(valueStr);
                    } else if(line.find("user0PosX") != -1) {
                        refScreenConfig.user0PosX = stof(valueStr);
                    } else if(line.find("user1PosX") != -1) {
                        refScreenConfig.user1PosX = stof(valueStr);
                    } else if(line.find("user2PosX") != -1) {
                        refScreenConfig.user2PosX = stof(valueStr);
                    } else {
                        cerr << "ERROR: Unrecognized value to assign " << lineNumber << ": " << line << endl;
                        parseWasSuccess = false;
                    }
                } else if (StringIsBool(valueStr)) {
                    if(line.find("PerPixelRays") != -1) {
                        bool raysPpixel = (valueStr == "true") ? true : false;
                        refScreenConfig.perPixelRays = raysPpixel;
                    }
                    if(line.find("StraightRays") != -1) {
                        bool straightRays = (valueStr == "true") ? true : false;
                        refScreenConfig.straightRays = straightRays;
                    }

                } else if (StringIsArray(valueStr)) {
                    if(line.find("PPL") != -1) {
                        //cout << "PPL:" << valueStr << endl;
                        refLensConfig.pixelsPerLenticuleCombinations = ParseToIntArray(valueStr);
                    } else if(line.find("SubstrateThicknesses") != -1) {
                        //cout << "ST: " << valueStr << endl;
                        refLensConfig.substrateThicknessCombinations = ParseToFloatArray(valueStr);
                    } else if(line.find("LensRadiuses") != -1) {
                        //cout << "LR: " << valueStr << endl;
                        refLensConfig.lensRadiusCombinations = ParseToFloatArray(valueStr);
                    } else {
                        cerr << "ERROR: Couldnt parse: " << valueStr << " as an array" << endl;
                        parseWasSuccess = false;
                    }
                } else {
                    cerr << "ERROR: Couldnt process line " << lineNumber << ": " << line << endl;
                    cerr << "Couldnt know its data type." << endl;
                    parseWasSuccess = false;
                }
            } else {
                cerr << "ERROR: Missing assignment operator '=' line " << lineNumber << ": " << line << endl;
                parseWasSuccess = false;
            }
        }
        lineNumber++;
    }
    /*if(parseWasSuccess) {
        cout << " = = = Loaded configuration = = = " << endl;
        cout << "Lenticular Sheet Config:" << endl;
        cout << " |->LenticularPitch:              " << refLensConfig.lenticularPitch << endl;
        cout << " |->LenticularRadius:             " << refLensConfig.lenticularRadius << endl;
        cout << " |->LenticularThickness:          " << refLensConfig.lenticularThickness << endl;
        cout << " |->LenticularSubstrateThickness: " << refLensConfig.lenticularSubstrateThickness << endl;
        cout << " |->LenticularLensRefraction:     " << refLensConfig.lenticularLensRefraction << endl << endl;
        cout << "Screen Config:" << endl;
        cout << " |->NumberOfPixels:     " << refScreenConfig.numberOfPixels << endl;
        cout << " |->SpaceBetweenPixels: " << refScreenConfig.spaceBetweenPixels << endl;
        cout << " |->PixelWidth:         " << refScreenConfig.pixelWidth << endl;
        cout << " |->PixelHeight:        " << refScreenConfig.pixelHeight << endl;
        cout << " |->RaysPerPixel:       " << refScreenConfig.raysPerPixel << endl;
        }*/
    return parseWasSuccess;
}

inline vector<float> Utils::ParseToFloatArray(const string &value) {
    vector<float> returnVec;
    vector<string> parsedStr = SplitToStringVec(value, ',');
    for(int i = 0; i < parsedStr.size(); i++) {
        returnVec.push_back(stof(parsedStr[i]));
    }
    return returnVec;
}

inline vector<int> Utils::ParseToIntArray(const string &value) {
    vector<int> returnVec;
    vector<string> parsedStr = SplitToStringVec(value, ',');
    for(int i = 0; i < parsedStr.size(); i++) {
        returnVec.push_back(stoi(parsedStr[i]));
    }
    return returnVec;
}

inline vector<string> Utils::SplitToStringVec(const string& s, char delimiter) {
   vector<string> tokens;
   string token;
   istringstream tokenStream(s);
   while (getline(tokenStream, token, delimiter)) {
      tokens.push_back(token);
   }
   return tokens;
}

inline bool Utils::StringIsArray(const string &value) {
    for(int i = 0; i < value.length(); i++) {
        if(value[i] == ',') {
            return true;
        }
    }
    return false;
}
inline bool Utils::StringIsFloat(const string &value) {
    for(int i = 0; i < value.length(); i++) {
        if(!isdigit(value[i]) && value[i] != '.' && value[i] != '-') {
            return false;
        }
    }
    return true;
}

inline bool Utils::StringIsBool(const string &value) {
    if(value == "true" || value == "false")
        return true;
    else
        return false;
}

/*inline bool Utils::StringIsInt(const string &value) {
    for(int i = 0; i < value.length(); i++) {
        if(!isdigit(value[i])) {
            return false;
        }
    }
    return true;
    }*/

//http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
// trim from start
inline void Utils::LTrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end
inline void Utils::RTrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends
inline void Utils::Trim(std::string &s) {
    LTrim(s);
    RTrim(s);
}

//took from: https://stackoverflow.com/questions/14369673/round-double-to-3-points-decimal
inline float Utils::RoundFloat(float val) {
    if( val < 0 ) return ceil(val - 0.5);
    return floor(val + 0.5);
}
