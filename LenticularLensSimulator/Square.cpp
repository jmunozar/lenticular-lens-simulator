#include "Square.h"

using namespace::std;

Square::Square(Camera &renderingCamera, GLuint _shaderProgramId, const glm::vec3 &position,
               float width, float height, const glm::vec4 &color) :
    camera {renderingCamera}, squareWidth {width}, squareHeight {height}, shaderProgramId {_shaderProgramId} {
        modelMatrix = glm::mat4(1.0f);
        modelMatrix = glm::translate(modelMatrix, position);

        squareColor[0] = color.r;
        squareColor[1] = color.g;
        squareColor[2] = color.b;
        squareColor[3] = color.a;

        GenerateSquareVertices();

        LoadSquareShader();
        InitializeSquareForRendering();
}

Square::~Square() {
    cout << "Cleaning up square primitive" << endl;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &verticesBufferHandle);
    Utils::ExitOnGLError("Couldnt destroy circle vertex array");

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &squareVAO);
    Utils::ExitOnGLError("Couldnt destroy square VAO");
}

// Top left corner is the position of the square
// +------------+
// |            |
// |            |
// |            |
// +------------+
//

void Square::GenerateSquareVertices() {
    glm::vec3 squarePos = modelMatrix * glm::vec4(0, 0, 0, 1);
    vertices[0] = glm::vec3(squarePos.x, squarePos.y, 0);
    vertices[1] = glm::vec3(squarePos.x + squareWidth, squarePos.y, 0);
    vertices[2] = glm::vec3(squarePos.x + squareWidth, squarePos.y - squareHeight, 0);
    vertices[3] = glm::vec3(squarePos.x, squarePos.y - squareHeight, 0);
}

void Square::LoadSquareShader() {
    projectionHandle = glGetUniformLocation(shaderProgramId, "projectionMatrix");
    viewHandle = glGetUniformLocation(shaderProgramId, "viewMatrix");
    modelHandle = glGetUniformLocation(shaderProgramId, "modelMatrix");
    colorHandle = glGetUniformLocation(shaderProgramId, "color");
    Utils::ExitOnGLError("Couldnt get shader uniforms for Model view and projection matrices for Square");
}

void Square::InitializeSquareForRendering() {
    glGenVertexArrays(1, &squareVAO);
    glBindVertexArray(squareVAO);

    //vertices
    glGenBuffers(1, &verticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    Utils::ExitOnGLError("ERROR: Couldn't create Vertex Array Object for square");
}

void Square::Render() {
    glUseProgram(shaderProgramId);

    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
    glProgramUniform4fv(shaderProgramId, colorHandle, 1, squareColor);

    glBindVertexArray(squareVAO);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertices.size());
    Utils::ExitOnGLError("Error: couldnt draw square.");
    glBindVertexArray(0);
}
