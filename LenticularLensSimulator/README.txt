This is a simple program that simulates light emmision (with rays) for lenticular lenses
from pixels to a lenticular screen.

INSTALLATION:
This program uses glm for mathematics and assimp for model importing
GLML:

install glm via brew (if running on OSX) and you should be good to go.

if not, download glm, place it in the same directory of the main file and change all the includes related to glm from <> to “” EG:
<glm/glm.hpp> —> “glm/glm.hpp”

Asset Importer (ASSIMP):
same deal, install via brew AND
modify the makefile to link against assimp with the “-lassimp” compiler flag.

The bitmap font generator for the text is done with: http://www.codehead.co.uk/cbfg/
