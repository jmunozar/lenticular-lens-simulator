#include "MenuApp.h"

using namespace::std;

MenuApp::MenuApp(GLFWwindow *windowRef, Camera &_cam, UserManager &_userMgr, LenticularConfig &_lensConfig, Screen &_screen, LenticularSheet &_lenticularSheet) :
    window {windowRef}, lensConfig {_lensConfig}, screen {_screen}, userMgr {_userMgr}, renderingCamera {_cam}, lenticularSheet {_lenticularSheet} {
        // Setup ImGui binding
        ImGuiBindingOnGLFWInit(window, true);
        ReloadMenuValues();
}

void MenuApp::Render() {
    DrawMenuWindow();
    if(displayAboutWindow)
        DrawAboutWindow();
}

void MenuApp::ReloadMenuValues() {
    menuRaysPerPixel = screen.GetScreenConfig().raysPerPixel;
    menuRaysTransparency = screen.GetScreenConfig().raysTransparency;
    menuPerPixelRays = screen.GetScreenConfig().perPixelRays;
    menuStraightRays = screen.GetScreenConfig().straightRays;
}

void MenuApp::DrawMenuWindow() {
    ImGuiWindowFlags windowFlags = 0;
    windowFlags |= ImGuiWindowFlags_MenuBar;//the window has a menu bar
    windowFlags |= ImGuiWindowFlags_NoMove;
    //windowFlags |= ImGuiWindowFlags_ShowBorders;

    ImGui::SetNextWindowPos(ImVec2(0,1));
    ImGui::Begin("Lenticular Lens Simulator", NULL/*window is NOT closable*/, windowFlags);
    // // // // // Menu Bar // // // // //
    if(ImGui::BeginMenuBar()) {
        if(ImGui::BeginMenu("Menu")) {
            if(ImGui::MenuItem("Quit", "esc"/*shortcut*/, false/*selected*/, true/*enabled*/)) {
                glfwSetWindowShouldClose(window, GL_TRUE);
            }
            ImGui::EndMenu();
        }
        if(ImGui::BeginMenu("Help")) {
            if(ImGui::MenuItem("About", NULL/*shortcut*/, false/*selected*/, true/*enabled*/)) {
                displayAboutWindow = true;
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
    // // // // // Menu Bar Ends // // // // //
    ImGui::BulletText("ASDW moves camera");
    ImGui::BulletText("Scroll to zoom");
    ImGui::BulletText("R reloads config file");

    ImGui::Separator(); // this is a: ------------------


    ImGui::PushItemWidth(ImGui::GetWindowWidth()*0.55f);//items have length of the whole window
    ImGui::Text("Lenticular Lens:");
    ImGui::Text("Drag values to modify them:");

    if(ImGui::DragFloat("Lens radius", &lensConfig.lenticularRadius, 0.1f, lensConfig.lenticularPitch/1.999f, 100 , "%.3f")) {
        simulationNeedsToBeUpdated = true;
    }
    float minPitch = ((screen.GetScreenConfig().pixelWidth+screen.GetScreenConfig().spaceBetweenPixels)*(float)screen.GetScreenConfig().numberOfPixels)/
        (float)lensConfig.lensesPerSheet;
    if(ImGui::DragFloat("Lens pitch", &lensConfig.lenticularPitch, 0.1f, minPitch, (lensConfig.lenticularRadius*1.999f),"%.3f")) {
        //we multiply the max value with 1.9999f (not with 2) to avoid errors when drawing the lens sheet
        simulationNeedsToBeUpdated = true;
    }
    if(ImGui::DragFloat("Substrate thickness", &lensConfig.lenticularSubstrateThickness, 0.01f, 0.1f, 10, "%.3f")) {
        simulationNeedsToBeUpdated = true;
    }
    if(ImGui::DragFloat("Refraction", &lensConfig.lenticularLensRefraction, 0.001f, 0.999f, 5 , "%.3f")) {
        simulationNeedsToBeUpdated = true;
    }

    ImGui::Separator();
    ImGui::Text("Screen:");
    if(ImGui::DragInt("Rays per pixel", &menuRaysPerPixel, 0.1f, 3, 100, "%.0f")) {//gets in when the value gets changed
        screen.SetRaysPerPixel(menuRaysPerPixel);
        simulationNeedsToBeUpdated = true;
    }
    if(ImGui::DragFloat("Rays transparency", &menuRaysTransparency, 0.01f, 0, 1, "%.2f")) {//gets in when the value gets changed
        screen.SetRaysTransparency(menuRaysTransparency);
        simulationNeedsToBeUpdated = true;
    }
    /*float spc = 0;
    if(ImGui::DragFloat("Space between pixels", &spc, 0.01f, 0, 1, "%.2f")) {//gets in when the value gets changed
        cout << "Call Callback" << endl;
        }*/
    /*float pw = 0.175;
    if(ImGui::DragFloat("Pixel width", &pw, 0.01f, 0, 1, "%.2f")) {//gets in when the value gets changed
        cout << "Call Callback" << endl;
        }*/

    if(ImGui::Checkbox("Per Pixel rays", &menuPerPixelRays)) {
        screen.SetPerPixelRays(menuPerPixelRays);
        simulationNeedsToBeUpdated = true;
    }
    //ImGui::SameLine(ImGui::GetWindowWidth()*0.55f);

    if(ImGui::Checkbox("Straight rays", &menuStraightRays)) {
        screen.SetStraightRays(menuStraightRays);
        simulationNeedsToBeUpdated = true;
    }

    if(ImGui::Button("Load Users")) {
        userMgr.ClearUsers();
        userMgr.AddUser(screen.GetScreenConfig().user0PosX);
        userMgr.AddUser(screen.GetScreenConfig().user1PosX);
        userMgr.AddUser(screen.GetScreenConfig().user2PosX);
    }
    if(ImGui::Button("Generate Lens Combinations")) {
        if(userMgr.GetUserCount() == 0) {
            cout << "Add at least 1 user for calculating lens combinations!" << endl;
            return;
        }

        clock_t begin = clock();
        ofstream reportFile;
        reportFile.open("ReportLensCombinations.txt");

        cout << "************************ Genereating lens combinations for: ************************" << endl;
        reportFile << "************************ Genereating lens combinations for: ************************" << endl;

        cout << "* Pixel Size: " << screen.GetScreenConfig().pixelWidth << endl;
        reportFile << "* Pixel Size: " << screen.GetScreenConfig().pixelWidth << endl;

        cout << "* Total pixels: " << screen.GetScreenConfig().numberOfPixels << endl;
        reportFile << "* Total pixels: " << screen.GetScreenConfig().numberOfPixels << endl;

        string s = "* Pixels per Lenticule:  ";
        for(int i = 0; i < lensConfig.pixelsPerLenticuleCombinations.size(); i++)
            s += to_string(lensConfig.pixelsPerLenticuleCombinations[i]) + ", ";

        cout << s << endl;
        reportFile << s << endl;

        s = "* Substrate thicknesess: ";
        for(int i = 0; i < lensConfig.substrateThicknessCombinations.size(); i++)
            s += to_string(lensConfig.substrateThicknessCombinations[i]) + ", ";

        cout << s << endl;
        reportFile << s << endl;

        s = "* Lens Radiuses:         ";
        for(int i = 0; i < lensConfig.lensRadiusCombinations.size(); i++)
            s += to_string(lensConfig.lensRadiusCombinations[i]) + ", ";
        cout << s << endl;
        reportFile << s << endl;

        string usersHeader = "";
        cout << "* User positions:" << endl;
        reportFile  << "* User positions:" << endl;

        for(int i = 0; i < userMgr.GetUserCount(); i++) {
            cout << "* User " << i << ": (" << userMgr.GetUser(i).GetPosition().x << ", " << userMgr.GetUser(i).GetPosition().y << ", " << userMgr.GetUser(i).GetPosition().z << ")" << endl;
            reportFile << "* User " << i << ": (" << userMgr.GetUser(i).GetPosition().x << ", " << userMgr.GetUser(i).GetPosition().y << ", " << userMgr.GetUser(i).GetPosition().z << ")\n";
            string strI = to_string(i);
            //%px_User es el porcentaje de la pantalla que le toca al usuario
            //%shared_px_user es el porcentaje de los pixeles que tiene el usuario i que esta compartidos con otros usuarios
            usersHeader += "px_User_" + strI + "(px) %px_User_" + strI + " Shared_px_User_" + strI + "(px) %shared_px_User_" + strI + " ";
        }
        cout << "* Distance users from screen:" << screen.GetScreenConfig().userDistanceFromScreen << endl;
        reportFile << "* Distance users from screen:" << screen.GetScreenConfig().userDistanceFromScreen << endl;
        cout << "* Rays per pixel:" << screen.GetScreenConfig().raysPerPixel << endl;
        reportFile  << "* Rays per pixel:" << screen.GetScreenConfig().raysPerPixel << endl;
        cout << "************************************************************************************" << endl;
        reportFile << "************************************************************************************" << endl;


        cout << "Lens_Radius(mm) Lens_Radius_Increment% Substr_Thickness(mm) Px_Pr_Lenticules(px) " << usersHeader<< endl;
        reportFile << "Lens_Radius(mm) Lens_Radius_Increment% Substr_Thickness(mm) Px_Pr_Lenticules(px) " << usersHeader<< endl;

        for(int i = 0; i < lensConfig.pixelsPerLenticuleCombinations.size(); i++) {
            int pixelsPerLenticuleI = lensConfig.pixelsPerLenticuleCombinations[i];
            lensConfig.lenticularPitch = ((float)pixelsPerLenticuleI) * screen.GetScreenConfig().pixelWidth;

            float minLensRadius = lensConfig.lenticularPitch/2 +0.000001;

            for(int j = 0; j < lensConfig.substrateThicknessCombinations.size(); j++) {
                float substrateThicknessJ = lensConfig.substrateThicknessCombinations[j];

                for(int k = 0; k < lensConfig.lensRadiusCombinations.size(); k++) {
                    float lensRadiusKPercentageMultiplier = lensConfig.lensRadiusCombinations[k];


                    lensConfig.lenticularRadius = minLensRadius * lensRadiusKPercentageMultiplier;//current radius
                    lensConfig.lenticularSubstrateThickness = substrateThicknessJ;


                    lenticularSheet.Initialize();
                    screen.Initialize();



                    string usersPixelHighlight = userMgr.HighlightPixelsForEachUser();

                    cout << lensConfig.lenticularRadius << " " << ((lensRadiusKPercentageMultiplier-1)*100) << "% " << substrateThicknessJ << " " << pixelsPerLenticuleI << " " << usersPixelHighlight << endl;
                    reportFile << lensConfig.lenticularRadius << " " << ((lensRadiusKPercentageMultiplier-1)*100) << "% " << substrateThicknessJ << " " << pixelsPerLenticuleI << " " << usersPixelHighlight << endl;
                }
            }
        }
        reportFile.close();
        clock_t end = clock();
        double elapsedSecs = double(end - begin) / CLOCKS_PER_SEC;
        cout << "Finished doing lens combinations! Time to process combinations: " << elapsedSecs << endl;
    }

    ImGui::Separator();
    string usersStr = "Users: " + std::to_string(userMgr.GetUserCount());
    ImGui::Text("%s", usersStr.c_str());

    if(ImGui::Button("-")) {
        if(userMgr.GetUserCount() > 0)
            userMgr.RemoveUser();
    }
    ImGui::SameLine();
    if(ImGui::Button("+")) {
        userMgr.AddUser();
    }

    if(ImGui::Button("Clear Users")) {
        userMgr.ClearUsers();
    }

    if(userMgr.GetUserCount() > 0)
        if(ImGui::Checkbox("Highlight pixels per user", &highlightPixelsPerUser)) {
            if(highlightPixelsPerUser) {
                userMgr.HighlightPixelsForEachUser();
            } else {
                screen.ResetPixelColors();
                screen.RenderPixelRays(true);
            }
        }
    ImGui::PopItemWidth();
    ImGui::End();



    /****** User movement ********/
    ImGuiIO& io = ImGui::GetIO();
    glm::vec3 mouseScreenPos = glm::vec3(io.MousePos.x, io.DisplaySize.y - io.MousePos.y, 0);
    glm::vec4 viewport = glm::vec4(0.0, 0.0, io.DisplaySize.x, io.DisplaySize.y);

    glm::vec3 mouseWorldPos = glm::unProject(mouseScreenPos,
                                             renderingCamera.GetViewMatrix(),
                                             renderingCamera.GetProjectionMatrix(),
                                             viewport);
    mouseWorldPos.z = 0;//we dont care about depth
    if(io.MouseClicked[0]) {//mouse down
        userMgr.SelectClosestUser(mouseWorldPos);
    }
    if(io.MouseDown[0]) {//left mouse btn pressed
        userMgr.MoveClosestUser(mouseWorldPos);
    }
    if(io.MouseReleased[0]) {//left mouse btn up
        userMgr.ClearSelectedUser();
        if(highlightPixelsPerUser)
            userMgr.HighlightPixelsForEachUser();
    }
    /* * * * * * * * * * * * * * */
}

void MenuApp::DrawAboutWindow() {
    int width = 0;
    int height = 0;
    glfwGetWindowSize(window, &width, &height);
    //TODO: Figure out why this is not centered!
    ImVec2 aboutWindowPosition = ImVec2((width-420)/2, (height-250)/2);//center of the screen

    ImGuiWindowFlags aboutWindowFlags = 0;
    aboutWindowFlags |= ImGuiWindowFlags_NoResize;
    aboutWindowFlags |= ImGuiWindowFlags_NoMove;
    aboutWindowFlags |= ImGuiWindowFlags_NoCollapse;
    aboutWindowFlags |= ImGuiWindowFlags_ShowBorders;


    ImGui::SetNextWindowPos(aboutWindowPosition);
    ImGui::SetNextWindowSize(ImVec2(420,250), ImGuiSetCond_FirstUseEver);

    ImGui::Begin("About", &displayAboutWindow, aboutWindowFlags);
    ImGui::Text("Created by: Juan Sebastian Munoz Arango");
    ImGui::Text("            naruse@gmail.com");
    ImGui::Spacing();
    ImGui::Text("Advisors:");
    ImGui::Text("            Dr. Dirk Reiners");
    ImGui::Text("            Dr. Carolina Cruz-neira");
    ImGui::Text("            EAC 2017 - Present");
    ImGui::End();
}
