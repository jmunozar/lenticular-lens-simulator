#include "User.h"

using namespace::std;

User::User(Camera &renderingCamera, GLuint _shaderProgramId, float _headSize,
           const glm::vec3 &position, const glm::vec4 &color) : headSize {_headSize}, userColor {color} {

    face.reset(new Circle(renderingCamera, _shaderProgramId, position, headSize, userColor));

    eyeSize = headSize/4;
    glm::vec3 eyePosL = position + glm::vec3(-headSize/2,
                                   -headSize/4,
                                   0.1);
    glm::vec3 eyePosR = position + glm::vec3(+headSize/2,
                                   -headSize/4,
                                   0.1f);

    leftEye.reset(new Circle(renderingCamera, _shaderProgramId,
                             eyePosL,
                             eyeSize,
                             glm::vec4(0,0,0,1)));
    rightEye.reset(new Circle(renderingCamera, _shaderProgramId,
                              eyePosR,
                              eyeSize,
                              glm::vec4(0,0,0,1)));
}

//destructor falta

void User::SetPosition(const glm::vec3 &newPos) {
    face->SetPosition(newPos);
    leftEye->SetPosition(newPos + glm::vec3(-headSize/2, -headSize/4, 0.1));
    rightEye->SetPosition(newPos + glm::vec3(+headSize/2, -headSize/4, 0.1f));
}

void User::Render() {
    face->Render();
    leftEye->Render();
    rightEye->Render();
}
