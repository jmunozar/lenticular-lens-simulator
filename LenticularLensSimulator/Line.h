#pragma once

#include <iostream>
#include <limits>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com


  Simple mathematical representation of the line equation.
 */

class Line {
 private:
    bool infiniteSlope = false;//special case
    float functionIfInfiniteSlope; //in case the slope is infinite, return the X value for the fn
    float m;
    float b;
 public:
    Line(const glm::vec2 &p1, const glm::vec2 &p2);
    Line(float m, float b);
    bool HasInfiniteSlope() const { return infiniteSlope; }
    float GetY(float x) const { return (m * x) + b; }
    float GetX(float y) const { return (y -b) / m; }
    float GetFunctionIfInfiniteSlope() const;
    float GetB() const;
    float GetM() const;
};
