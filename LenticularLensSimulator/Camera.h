#pragma once

#include "Utils.h"

#include <iostream>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com
 */

class Camera {
 private:
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;


    float size = 150;
    float nearPlaneDistance = -100.1f;
    float farPlaneDistance = 10000;

    float speed = 60.0f;
    float zoomSensitivity = 1.0f;
    GLFWwindow* window;
 public:
    void GetInput(float scrollDelta);

    glm::mat4 GetViewMatrix() { return viewMatrix; }
    glm::mat4 GetProjectionMatrix() { return projectionMatrix; }

    Camera(GLFWwindow* windowRef);

    glm::vec3 position = glm::vec3(90,90,100);
};
