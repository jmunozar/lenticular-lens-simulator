#pragma once

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This is a simple class that renders the menu via imgui for the
  simulator. values hare are tweakable and affect the simulation.
 */

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>
#include <ctime>

#include <iomanip>
#include "imgui/imgui.h"
#include "ImGuiBindingOnGLFW.h"
#include "ConfigStructures.h"
#include "UserManager.h"
#include "Camera.h"


class MenuApp {
 private:
    GLFWwindow* window;
    LenticularConfig& lensConfig;
    Screen& screen;
    UserManager& userMgr;
    Camera& renderingCamera;
    LenticularSheet& lenticularSheet;

    bool simulationNeedsToBeUpdated = false;
    bool displayAboutWindow = false;
    bool highlightPixelsPerUser = false;

    //screen cfg vars
    int menuRaysPerPixel;
    float menuRaysTransparency;
    bool menuPerPixelRays;
    bool menuStraightRays;

    void DrawMenuWindow();
    void DrawAboutWindow();
 public:
    MenuApp(GLFWwindow* windowRef, Camera &_cam, UserManager &userMgr, LenticularConfig &_lensConfig, Screen &_screen, LenticularSheet &_lenticularSheet);
    ~MenuApp() {
        std::cout << "Cleaning up menu." << std::endl;
    }
    void SetupNewFrame() { ImGuiBindingOnGLFWNewFrame(); }
    void Cleanup() { ImGuiBindingOnGLFWShutdown(); }
    void Render();

    void ReloadMenuValues();

    bool NeedToUpdateSimulation() {
        if(simulationNeedsToBeUpdated) {
            simulationNeedsToBeUpdated = false;
            return true;
        }
        return false;
    }
};
