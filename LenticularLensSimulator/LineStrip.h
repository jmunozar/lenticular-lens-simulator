#ifndef LINESTRIP_H
#define LINESTRIP_H

#include <GL/glew.h>

#include <iostream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"
#include "Utils.h"

class LineStrip {
 private:
    Camera& camera;//<--rendering camera
    glm::mat4 modelMatrix = glm::mat4(1);
    float lineColor[4];

    std::vector<glm::vec3> vertices;

    //glsl handles
    GLuint projectionHandle;
    GLuint viewHandle;
    GLuint modelHandle;
    GLuint colorHandle;

    GLuint verticesBufferHandle;
    GLuint lineShaderProgramId;
    GLuint lineVAO;

    void LoadLineStripShader();
    void InitializeLineStripForRendering();

 public:
    LineStrip(Camera &renderingCamera, GLuint _lineShaderProgramId, const std::vector<glm::vec3> &points, const glm::vec4 &color);
    LineStrip(const LineStrip& source) = delete;//means this class cannot be copy constructed
    LineStrip& operator=(const LineStrip& source) = delete;//means this class cannot be copy assigned
    ~LineStrip();

    void Render();
    void DestroyLineStrip();

    const glm::vec3& GetEndPoint() { return vertices[vertices.size()-1]; }
};
#endif
