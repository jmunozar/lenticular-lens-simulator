#version 400

in vec4 ex_color;
out vec4 out_Color;

void main() {
    out_Color = ex_color;
}
