#include "LineStrip.h"

using namespace::std;

LineStrip::LineStrip(Camera& renderingCamera, GLuint _lineShaderProgamId,
                     const vector<glm::vec3> &points, const glm::vec4 &color)
    : camera {renderingCamera}, modelMatrix {glm::mat4(1)}, vertices {points}, lineShaderProgramId {_lineShaderProgamId} {
        lineColor[0] = color.r;
        lineColor[1] = color.g;
        lineColor[2] = color.b;
        lineColor[3] = color.a;

        LoadLineStripShader();
        InitializeLineStripForRendering();
}

LineStrip::~LineStrip() {
    DestroyLineStrip();
}

void LineStrip::LoadLineStripShader() {
    projectionHandle = glGetUniformLocation(lineShaderProgramId, "projectionMatrix");
    viewHandle = glGetUniformLocation(lineShaderProgramId, "viewMatrix");
    modelHandle = glGetUniformLocation(lineShaderProgramId, "modelMatrix");
    colorHandle = glGetUniformLocation(lineShaderProgramId, "color");
    Utils::ExitOnGLError("Couldnt get shader uniforms for Model view and projection matrices");
}
void LineStrip::InitializeLineStripForRendering() {
    glGenVertexArrays(1, &lineVAO);
    glBindVertexArray(lineVAO);

    //vertices
    glGenBuffers(1, &verticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    Utils::ExitOnGLError("ERROR: Couldn't create Vertex Array Object for line");
}


void LineStrip::Render() {
    glUseProgram(lineShaderProgramId);

    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
    glProgramUniform4fv(lineShaderProgramId, colorHandle, 1, lineColor);

    glBindVertexArray(lineVAO);
    glDrawArrays(GL_LINE_STRIP, 0, vertices.size());
    Utils::ExitOnGLError("Error: couldnt draw line.");
    glBindVertexArray(0);
}

void LineStrip::DestroyLineStrip() {
    //destroy texture
    //no need as model doesnt have texture ;-)
    //destroy model
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &verticesBufferHandle);
    Utils::ExitOnGLError("Couldnt destroy line vertex array");

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &lineVAO);
    Utils::ExitOnGLError("Couldnt destroy line VAO");
}
