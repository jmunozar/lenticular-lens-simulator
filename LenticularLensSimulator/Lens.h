#ifndef LENS_H
#define LENS_H

#include <GL/glew.h>

#include <iostream>
#include <vector>
#include <math.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"

class Lens {
 private:
    Camera& camera;
    glm::mat4 modelMatrix;
    glm::vec3 position;//center of the lens

    float lensColorArr[4];
    std::vector<glm::vec3> vertices;

    //glsl handles
    GLuint projectionHandle;
    GLuint viewHandle;
    GLuint modelHandle;
    GLuint colorHandle;

    GLuint shaderProgramId;
    GLuint lensVAO;
    GLuint verticesBufferHandle;

    void LoadShaderValues();
    void InitializeLensForRendering();
    void GenerateVertices(float lensRadius, float lensAngle);
    void DestroyLens();

 public:
    Lens(Camera &renderingCamera, const glm::vec3 &pos, const glm::vec4 &color,
         float radius, float lensAngle, GLuint _shaderProgramId);
    Lens(Lens const& source) = delete;//<-- means this class cannot be copy constructed
    Lens& operator=(Lens const& source) = delete;//<-- Lens cannot be copy assigned
    ~Lens();
    void Render();
};
#endif
