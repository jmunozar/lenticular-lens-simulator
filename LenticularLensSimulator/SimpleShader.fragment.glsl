#version 400

in vec2 vertexUV;
in vec3 vertexPosWorldSpace;
in vec3 normalCamSpace;
in vec3 eyeDirCamSpace;
in vec3 lightDirCamSpace;

out vec4 out_Color;

uniform sampler2D texSampler;
uniform vec3 lightPosWorldSpace;
uniform float transparency;

void main(void) {
    //probar tener variables fuera del main.
    //think about moving this as uniforms
    vec3 lightColor = vec3(1,1,1);
    float lightPwr = 10000;

    //mat properties
    vec3 materialDiffuseColor = texture(texSampler, vertexUV).rgb;
    vec3 materialAmbientColor = vec3(0.1, 0.1, 0.1) * materialDiffuseColor;
    vec3 materialSpecularColor= vec3(0.3, 0.3, 0.3);

    //distance to light
    float distanceToLight = length(lightPosWorldSpace - vertexPosWorldSpace);

    //normal of the computed fragment in cam space     //think of changin this to a better name
    vec3 n = normalize(normalCamSpace);

    //light dir in cam space normalized
    vec3 l = normalize(lightDirCamSpace);

    float cosTetha = clamp(dot(n,l), 0, 1);//clamp only to 0-1 bc we dont care if the light is behind the vertex actually...

    //eye vec towards the camera (from vertex to cam)
    vec3 E = normalize(eyeDirCamSpace);

    //direction of the light reflected
    vec3 R = reflect(-l, n);

    float cosAlpha = clamp(dot(E,R), 0, 1);

    //out_Color = texture(texSampler, vertexUV).rgb;


    out_Color = vec4((
        //Ambient: Simulates indirect lighting
        materialAmbientColor +
        //diffues: "color of the object"
        materialDiffuseColor * lightColor * lightPwr * cosTetha / (distanceToLight*distanceToLight) +
        //specular: reflective highlight (like a mirror)
        materialSpecularColor * lightColor * lightPwr * pow(cosAlpha,5/*100*/) / (distanceToLight*distanceToLight)), transparency);
}
