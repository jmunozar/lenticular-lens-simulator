#include "Debug.h"

using namespace::std;

void Debug::DrawX(const glm::vec3 &pos, const glm::vec4 &color) {
    vector<glm::vec3> crossLine;
    crossLine.push_back(pos - glm::vec3(crossSize/2, crossSize/2, 0));
    crossLine.push_back(pos + glm::vec3(crossSize/2,crossSize/2,0));
    lines.emplace_back(new LineStrip(*cam, shaderHandle, crossLine, color));
    crossLine.clear();
    crossLine.push_back(pos - glm::vec3(crossSize/2, crossSize/2, 0) + glm::vec3(0, crossSize, 0));
    crossLine.push_back(pos - glm::vec3(crossSize/2, crossSize/2, 0) + glm::vec3(crossSize, 0, 0));
    lines.emplace_back(new LineStrip(*cam, shaderHandle, crossLine, color));
}

void Debug::DrawLine(const glm::vec3 &start, const glm::vec3 &end, const glm::vec4 &color) {
    vector<glm::vec3> line;
    line.push_back(start);
    line.push_back(end);
    lines.emplace_back(new LineStrip(*cam, shaderHandle, line, color));
}

void Debug::DrawLine(const glm::vec2 &start, const glm::vec2 &end, const glm::vec4 &color) {
    DrawLine(glm::vec3(start.x, start.y, 0), glm::vec3(end.x, end.y, 0), color);
}

void Debug::RenderDebugInfo() {
    for(int i = 0; i < lines.size(); i++) {
        lines[i]->Render();
    }
}

void Debug::Initialize(Camera &_cam, GLuint _shaderHandle, float _crossSize) {
    cam = &_cam;
    shaderHandle = _shaderHandle;
    crossSize = _crossSize;
}
