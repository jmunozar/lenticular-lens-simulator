#version 400

in vec2 UV;

out vec4 color;

uniform sampler2D glyphsTexture;

void main() {
    color = texture(glyphsTexture, UV);
}
