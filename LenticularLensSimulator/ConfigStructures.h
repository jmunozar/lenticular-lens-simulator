#pragma once

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This is a simple file that contains structures with information read from
  the Config.txt file. This information is read in Utils.cpp
 */

#include <glm/glm.hpp>

using namespace::std;

//config values for lenticular sheet
struct LenticularConfig {
    float lenticularPitch;//width each lenticular cell
    float lenticularRadius;//radius of the lenticule
    //float lenticularThickness;
    float lenticularSubstrateThickness;
    float lenticularLensRefraction;
    int lensesPerSheet;

    //This is for batch comparison of different types of lenses and combinations
    vector<int> pixelsPerLenticuleCombinations;
    vector<float> substrateThicknessCombinations;
    vector<float> lensRadiusCombinations;
};

//config values for pixels
struct ScreenConfig {
    int numberOfPixels;
    float spaceBetweenPixels;
    float pixelWidth;
    float pixelHeight;
    int raysPerPixel;
    bool perPixelRays;
    bool straightRays;
    float raysTransparency;
    float userDistanceFromScreen;

    float user0PosX;
    float user1PosX;
    float user2PosX;
};
