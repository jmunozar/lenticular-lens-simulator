#This is a comment
# For a better understanding: https://en.wikipedia.org/wiki/Lenticular_lens
# measures are in millimiters


##########################
# Lenticular Lens Config #
##########################
# VALUES FOR 10LPI
# Lens pitch: 0.1in -> 2.54mm

#not in mm
LensesPerSheet = 1010

#width of each lenticular cell
LenticularPitch = 1.27

#Radius of curvature of the lenticule
LenticularRadius = 1.61

#Thickness of the whole sheet
#LenticularThickness = 3.26

#Thickness until the lenticules start (Yc)
LenticularSubstrateThickness = 2.85

#lens's index of refraction
LenticularLensRefraction = 1.47
#refraction of air = 1.00


############################
#  Screen specific config  #
############################
### Screen values for ASUS VN 247 ###
#2560
NumberOfPixelsInScreen = 3840

#2.5m from screen = 2500
UserDistanceFromScreen = 1000

#This is a float value
SpaceBetweenPixels = 0

###########################
#  Pixel specific config  #
###########################
#measurements in mm
PixelWidth = 0.315037978
PixelHeight = 0.315037978
RaysPerPixel = 25

#either color each pixel rays or paint them with red blue green rays for each subpixel
PerPixelRays = true
#rays from pixel are straight or follow a rainbow shape.
StraightRays = true
#goes from [0-1] 0: fully transparent, 1: opaque
RaysTransparency = 0.3

###########################
#   Ray specific config   #
###########################
