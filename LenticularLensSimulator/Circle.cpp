#include "Circle.h"

#include <GLFW/glfw3.h>

using namespace::std;

Circle::Circle(Camera &renderingCamera, GLuint _shaderProgramId, const glm::vec3 &_center,
               float _radius, const glm::vec4 color)
    : radius {_radius}, camera {renderingCamera}, shaderProgramId {_shaderProgramId}  {

        modelMatrix = glm::mat4(1.0f);
        modelMatrix = glm::translate(modelMatrix, _center);

        circleColor[0] = color.r;
        circleColor[1] = color.g;
        circleColor[2] = color.b;
        circleColor[3] = color.a;

        GenerateCircleVertices();

        LoadCircleShader();
        InitializeCircleForRendering();

}

Circle::~Circle() {
    cout << "Cleaning up circle primitive" << endl;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &verticesBufferHandle);
    Utils::ExitOnGLError("Couldnt destroy circle vertex array");

    glBindVertexArray(0);
    glDeleteVertexArrays(1, &circleVAO);
    Utils::ExitOnGLError("Couldnt destroy circle VAO");
}

glm::vec3 Circle::GetPosition() {
    glm::vec4 pos = modelMatrix * glm::vec4(0, 0, 0, 1);
    //pos.xyz; <-- check swizzling on glm
    return glm::vec3(pos.x, pos.y, pos.z);
}

void Circle::SetPosition(const glm::vec3 &newPos) {
    modelMatrix = glm::mat4(1.0f);
    modelMatrix = glm::translate(modelMatrix, newPos);
}

 void Circle::GenerateCircleVertices() {
     vertices.clear();
     float fragments = 50;

     float increment = 2.0f * glm::pi<float>() / fragments;

     for (float currAngle = 0.0f; currAngle <= 2.0f * glm::pi<float>(); currAngle += increment) {
         vertices.push_back(glm::vec3(radius * cos(currAngle),
                                      radius * sin(currAngle),
                                      0));
     }
}

void Circle::LoadCircleShader() {
    projectionHandle = glGetUniformLocation(shaderProgramId, "projectionMatrix");
    viewHandle = glGetUniformLocation(shaderProgramId, "viewMatrix");
    modelHandle = glGetUniformLocation(shaderProgramId, "modelMatrix");
    colorHandle = glGetUniformLocation(shaderProgramId, "color");
    Utils::ExitOnGLError("Couldnt get shader uniforms for Model view and projection matrices for Circle");
}

void Circle::InitializeCircleForRendering() {
    glGenVertexArrays(1, &circleVAO);
    glBindVertexArray(circleVAO);

    //vertices
    glGenBuffers(1, &verticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    Utils::ExitOnGLError("ERROR: Couldn't create Vertex Array Object for circle");
}

void Circle::Render() {
    glUseProgram(shaderProgramId);

    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
    glProgramUniform4fv(shaderProgramId, colorHandle, 1, circleColor);

    glBindVertexArray(circleVAO);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertices.size());
    Utils::ExitOnGLError("Error: couldnt draw circle.");
    glBindVertexArray(0);
}
