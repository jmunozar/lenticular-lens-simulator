/*
  Created by
  Juan Sebastian Munoz Arango
  naruse@gmail.com
 */

#include "UserManager.h"

using namespace::std;

UserManager::UserManager(Camera &cam, GLuint _shaderProgramId,  Screen &_screen) :
    camera {cam}, shaderProgramId {_shaderProgramId}, screen {_screen} {

}

void UserManager::RenderUsers() {
    for(int i = 0; i < users.size(); i++)
        users[i]->Render();
}

void UserManager::AddUser(float xPos) {
    float headSizeRadius = 160;// FIX THIS, should be 281; // -> 562.0/2.0//https://en.wikipedia.org/wiki/Human_head avg 55.2cm females, 57.2 males hence avg of both = 56.2
    float yPos = screen.GetScreenConfig().userDistanceFromScreen + headSizeRadius/4;/* headSizeRadius to align with eyes*/
    glm::vec3 userPosition = glm::vec3(xPos, yPos, 0);
    users.emplace_back(new User(camera, shaderProgramId, headSizeRadius, userPosition, Utils::GenerateRandomColor()));
}
void UserManager::RemoveUser() {
    users.pop_back();
}

void UserManager::ClearUsers() {
    while(users.size() > 0)
        users.pop_back();
}

//selects the 1rst user that appears
void UserManager::SelectClosestUser(const glm::vec3 &mouseWorldPos) {
    for(int i = 0; i < users.size(); i++) {
        if(glm::length(mouseWorldPos - users[i]->GetPosition()) < users[i]->GetHeadSize()) {
            selectedUserIndex = i;
            return;
        }
    }
    selectedUserIndex = -1;//no user found
}

void UserManager::MoveClosestUser(const glm::vec3 &mouseWorldPos) {
    if(selectedUserIndex == -1)//no user found to move
        return;
    float yPos = screen.GetScreenConfig().userDistanceFromScreen + users[selectedUserIndex]->GetHeadSize()/4;
    glm::vec3 restrictedPos = glm::vec3(mouseWorldPos.x, yPos, 0);
    users[selectedUserIndex]->SetPosition(restrictedPos);
}


//returns a string with the pixels the user has + (% of the screen), shared pixels of the pixels the user has and the percentage of the shared pixel among the pixels he has from the screen
//for the number of users that are in the screen
string UserManager::HighlightPixelsForEachUser() {
    screen.RenderPixelRays(false);//lets disable all the pixel rays from the
                                  //screen and only enable the ones that
                                  //touches user eyes
    vector<vector<int> > sharedPixels;

    //cout << "==================================" << endl;
    //cout << "= Total pixels in screen: " << screen.GetScreenConfig().numberOfPixels << endl;
    for(int i = 0; i < users.size(); i++) {
        vector<int> pixelsUserI;
        sharedPixels.push_back(pixelsUserI);

        float eyesSize = users[i]->GetEyeSize();
        glm::vec3 userEyePosL = users[i]->GetEyePosL();
        glm::vec3 userEyePosR = users[i]->GetEyePosR();



        for(int j = 0; j < screen.GetScreenPixels().size(); j++) {
            sharedPixels[i].push_back(0);

            for(int k = 0; k < screen.GetScreenPixels()[j]->GetPixelRays().size(); k++) {
                glm::vec3 endPointRay = screen.GetScreenPixels()[j]->GetPixelRays()[k]->GetEndPoint();
                float distToLEye = glm::length(userEyePosL-endPointRay);
                float distToREye = glm::length(userEyePosR-endPointRay);
                if(distToLEye <= eyesSize || distToREye <= eyesSize) {//ray from pixel touches the eye
                    screen.GetScreenPixels()[j]->RenderPixelRays(true);
                    screen.GetScreenPixels()[j]->SetPixelColor(users[i]->GetUserColor());

                    sharedPixels[i][j]++;

                    break;
                }
            }
        }


    }

    string resultUsers = "";

    for(int i = 0; i < sharedPixels.size(); i++) {
        int pixelsForUserI = 0;
        //cout << "User " << i << " => ";
        int sharedPixelsForUser = 0;
        for(int j = 0; j < sharedPixels[i].size(); j++) {
            //cout << sharedPixels[i][j] << " ";
            int pixelShared = 0;

            if(sharedPixels[i][j] == 1) {
                for(int k = 0; k < sharedPixels.size(); k++) {
                    if(k!=i)
                        pixelShared += sharedPixels[k][j];
                }
            }
            if(pixelShared > 0)
                sharedPixelsForUser++;
            pixelsForUserI += sharedPixels[i][j];
        }
        //cout << endl;
        float pixelPercentage = ((100.0 * (float)pixelsForUserI)/(float)screen.GetScreenConfig().numberOfPixels);
        float sharedPixelPercentage = ((100.0*(float)sharedPixelsForUser)/(float)pixelsForUserI);
        //cout << "User: " << i << " has " << pixelsForUserI << "px (" << pixelPercentage << "\\%) & Shared pixels: " << sharedPixelsForUser << "px (" << sharedPixelPercentage << "\\%) &"<< endl;

        //pixels userI percentageFromScreenForPixelsUserI sharedPixelsUserI percentageOfSharedPixelsFromUserI
        resultUsers += to_string(pixelsForUserI) + " " + to_string(pixelPercentage) + "% " + to_string(sharedPixelsForUser) + " " + to_string(sharedPixelPercentage) + "% ";
    }

    return resultUsers;
}
