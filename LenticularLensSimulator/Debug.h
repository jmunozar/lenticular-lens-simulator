#ifndef DEBUG_H
#define DEBUG_H

#include <iostream>
#include <vector>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"
#include "LineStrip.h"

/*
  This class  serves for drawing X's to debug
  values graphically, doesnt do anything else

*/

class Debug {
 private:
    Debug() {}
    Debug(Debug const& s);
    void operator=(Debug const& s);
    std::vector<std::unique_ptr<LineStrip>> lines;
    Camera *cam;//using a normal pointer instead of a unique because we dont "own" the camera object (that's what unique pointers do) we just need its reference
    GLuint shaderHandle;
    float crossSize;
 public:
    static Debug& GetInstance() {
        static Debug instance;
        return instance;
    }
    void Initialize(Camera &cam, GLuint _shaderHandle, float _crossSize = 10);
    void RenderDebugInfo();

    void DrawX(const glm::vec3 &pos, const glm::vec4 &color = glm::vec4(1,1,1,1));
    void DrawLine(const glm::vec3 &start, const glm::vec3 &end, const glm::vec4 &color = glm::vec4(1,1,1,1));
    void DrawLine(const glm::vec2 &start, const glm::vec2 &end, const glm::vec4 &color = glm::vec4(1,1,1,1));


};
#endif

