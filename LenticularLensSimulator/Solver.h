#pragma once

#include <iostream>
#include <vector>
#include <limits>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "ConfigStructures.h"
#include "LenticularSheet.h"
#include "Debug.h"
#include "Line.h"

/*
  Create by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This class is a singleton, this class is used to solve refraction on each
  ray from the lenticular lenses sheet
*/
//class Line;
class Solver {
 private:
    Solver() {}
    Solver(Solver const& s);
    void operator=(Solver const& s);

    bool LinesCross(const Line &l1, const Line &l2, glm::vec2 &crossPoint);
    bool LineLensIntersect(const Line &l1, const glm::vec2 &lensCenter, float radius, glm::vec2 &intersectionPoint);
    //Line GetTangentFromCircleAtPoint(const glm::vec2 &pointToGetTangent, const glm::vec2 &lensCenter);
    glm::vec2 GetRefractedEndPoint(const glm::vec2 &rayOriginPos, const glm::vec2 &rayLensIntersection, const glm::vec2 &lensCenter, double lensRefractionIndex);
 public:
    static Solver& GetInstance() {
        static Solver instance;
        return instance;
    }
    std::vector<glm::vec3> CalculateRayTrajectory(const LenticularConfig &lensConfig, const LenticularSheet &lensSheet,
                                                  const glm::vec3 &rayPos, const glm::vec3 &rayDir, float userDistanceFromScreen);

};
