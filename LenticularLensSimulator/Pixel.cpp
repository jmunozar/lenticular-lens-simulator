#include "Pixel.h"

using namespace::std;

Pixel::Pixel(Camera &_renderingCamera, const glm::vec3 &_pos, float _width, float _height, int _raysPerPixel,
             const LenticularConfig &lensConfig, const ScreenConfig &screenConfig,
             const LenticularSheet &lensSheet, GLuint _shaderProgramId, const glm::vec4 &_color)
    : camera {_renderingCamera}, modelMatrix {glm::mat4(1)},
      position{_pos}, width {_width}, height {_height}, raysPerPixel {_raysPerPixel},
      shaderProgramId {_shaderProgramId} {

        pixelColorArr[0] = _color.r; pixelColorArr[1] = _color.g; pixelColorArr[2] = _color.b; pixelColorArr[3] = _color.a;
        //subpixels use same alpha from background
        redSubPixelColorArr[3] = _color.a; greenSubPixelColorArr[3] = _color.a; blueSubPixelColorArr[3] = _color.a;

        GeneratePixelVertices();

        LoadShaderValues();

        //rays color not used if perPixelRays is set in the menu app / Config.txt
        GenerateRaysForPixel(lensConfig, screenConfig, lensSheet, Utils::GenerateRandomColor(screenConfig.raysTransparency));

        InitializePixelForRendering();
}

Pixel::~Pixel() {
    DestroyPixel();
}

void Pixel::LoadShaderValues() {
    /*projectionHandle = glGetUniformLocation(shaderProgramId, "projectionMatrix");
    viewHandle = glGetUniformLocation(shaderProgramId, "viewMatrix");
    modelHandle = glGetUniformLocation(shaderProgramId, "modelMatrix");
    colorHandle = glGetUniformLocation(shaderProgramId, "color");

    Utils::ExitOnGLError("Couldnt get shader uniforms for Model view and projection matrices for pixel");*/
}
void Pixel::InitializePixelForRendering() {
    /*//pixel background
    glGenVertexArrays(1, &pixelBackgroundVAO);
    glBindVertexArray(pixelBackgroundVAO);
    //vertices
    glGenBuffers(1, &verticesBackgroundBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, verticesBackgroundBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, verticesPixelBackground.size() * sizeof(glm::vec3), &verticesPixelBackground[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(0);//this enables the attribute set in the last line (glVertexAttribPointer)

    //Lets not draw subpixel colors
    //red sub pixel
    glGenVertexArrays(1, &redSubPixelVAO);
    glBindVertexArray(redSubPixelVAO);
    //vertices
    glGenBuffers(1, &redSubPixelVerticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, redSubPixelVerticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, verticesRedSubPixel.size() * sizeof(glm::vec3), &verticesRedSubPixel[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(0);

    //green sub pixel
    glGenVertexArrays(1, &greenSubPixelVAO);
    glBindVertexArray(greenSubPixelVAO);
    //vertices
    glGenBuffers(1, &greenSubPixelVerticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, greenSubPixelVerticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, verticesGreenSubPixel.size() * sizeof(glm::vec3), &verticesGreenSubPixel[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(0);

    //blue sub pixel
    glGenVertexArrays(1, &blueSubPixelVAO);
    glBindVertexArray(blueSubPixelVAO);
    //vertices
    glGenBuffers(1, &blueSubPixelVerticesBufferHandle);
    glBindBuffer(GL_ARRAY_BUFFER, blueSubPixelVerticesBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, verticesBlueSubPixel.size() * sizeof(glm::vec3), &verticesBlueSubPixel[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(0);


    glBindVertexArray(0);
    Utils::ExitOnGLError("ERROR: Couldn't create Vertex Array Object for
    pixel");
    */
}

void Pixel::Render() {
    /*glUseProgram(shaderProgramId);

    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
    glProgramUniform4fv(shaderProgramId, colorHandle, 1, pixelColorArr);

    glBindVertexArray(pixelBackgroundVAO);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    Utils::ExitOnGLError("Error: couldnt draw pixel.");
    glBindVertexArray(0);


    //red subpixel
    //no need to set the glUniformMatrix for each color as we set them
    //already with the bg and is the same program
    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
       glProgramUniform4fv(shaderProgramId, colorHandle, 1, redSubPixelColorArr);

       glBindVertexArray(redSubPixelVAO);
       glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
       Utils::ExitOnGLError("Error: couldnt draw red sub pixel.");
       glBindVertexArray(0);

    //green subpixel
    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
       glProgramUniform4fv(shaderProgramId, colorHandle, 1, greenSubPixelColorArr);

       glBindVertexArray(greenSubPixelVAO);
       glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
       Utils::ExitOnGLError("Error: couldnt draw green sub pixel.");
       glBindVertexArray(0);

    //blue subpixel
    glUniformMatrix4fv(projectionHandle, 1, GL_FALSE, &camera.GetProjectionMatrix()[0][0]);
    glUniformMatrix4fv(viewHandle, 1, GL_FALSE, &camera.GetViewMatrix()[0][0]);
    glUniformMatrix4fv(modelHandle, 1, GL_FALSE, &modelMatrix[0][0]);
       glProgramUniform4fv(shaderProgramId, colorHandle, 1, blueSubPixelColorArr);

       glBindVertexArray(blueSubPixelVAO);
       glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
       Utils::ExitOnGLError("Error: couldnt draw blue sub pixel.");
       glBindVertexArray(0);


    //blending
    glEnable(GL_BLEND);//this is to make the rays additive when they overlap and not blend in
                       //any other part of the program
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    if(renderRays) {
        for(int i = 0; i < rays.size(); i++)
            rays[i]->Render();//<--- Este es un problema de performance;
                              //idealmente renderizar TODOS los rayos al
                              //mismo tiempo
    }
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    */

}

void Pixel::GeneratePixelVertices() {
    /*
    //background
    verticesPixelBackground[0] = position;
    verticesPixelBackground[1] = position + glm::vec3(width, 0, 0);
    verticesPixelBackground[2] = position + glm::vec3(width, -height, 0);//-height as it goes down
    verticesPixelBackground[3] = position + glm::vec3(0, -height, 0);

    float subPixelWidth = width*0.26f;
    float subPixelHeight = height * 0.85f;
    //red subpixel
    glm::vec3 redSubPixelPos = position + glm::vec3(width * 0.05f,-height * 0.05f,0.1f);
    verticesRedSubPixel[0] = redSubPixelPos;
    verticesRedSubPixel[1] = redSubPixelPos + glm::vec3(subPixelWidth, 0, 0);
    verticesRedSubPixel[2] = redSubPixelPos + glm::vec3(subPixelWidth, -subPixelHeight, 0);
    verticesRedSubPixel[3] = redSubPixelPos + glm::vec3(0, -subPixelHeight, 0);

    //green subpixel
    glm::vec3 greenSubPixelPos = position + glm::vec3((width*0.05f)*2 + width*0.27f, -height*0.05f,0.1f);
    verticesGreenSubPixel[0] = greenSubPixelPos;
    verticesGreenSubPixel[1] = greenSubPixelPos + glm::vec3(subPixelWidth, 0, 0);
    verticesGreenSubPixel[2] = greenSubPixelPos + glm::vec3(subPixelWidth, -subPixelHeight, 0);
    verticesGreenSubPixel[3] = greenSubPixelPos + glm::vec3(0, -subPixelHeight, 0);

    //blue subpixel
    glm::vec3 blueSubPixelPos = position + glm::vec3((width*0.05f)*3 + (width*0.27f)*2, -height*0.05f,0.1f);
    verticesBlueSubPixel[0] = blueSubPixelPos;
    verticesBlueSubPixel[1] = blueSubPixelPos + glm::vec3(subPixelWidth, 0, 0);
    verticesBlueSubPixel[2] = blueSubPixelPos + glm::vec3(subPixelWidth, -subPixelHeight, 0);
    verticesBlueSubPixel[3] = blueSubPixelPos + glm::vec3(0, -subPixelHeight,
    0);
    */
}

void Pixel::GenerateRaysForPixel(const LenticularConfig &lensConfig, const ScreenConfig &screenConfig,
                                 const LenticularSheet &lensSheet, const glm::vec4 &raysColor) {
    float step = (float)width/((float)raysPerPixel-1);

    glm::vec4 rayColor = raysColor;

    for(int i = 0; i < raysPerPixel; i++) {
        float rayPositionX = (float)i*step;
        glm::vec3 rayStart = position + glm::vec3(rayPositionX, 0, 0);//starting pos
        glm::vec3 rayDirection = glm::normalize(glm::vec3(0, 1, 0));//direction

        if(screenConfig.perPixelRays) {
            //float rayPositionX = (float)i*step;
            if(rayPositionX < width)      rayColor = glm::vec4(0,0,1, screenConfig.raysTransparency);
            if(rayPositionX < width*0.66) rayColor = glm::vec4(0,1,0, screenConfig.raysTransparency);
            if(rayPositionX < width*0.33) rayColor = glm::vec4(1,0,0, screenConfig.raysTransparency);
        }

        if(!screenConfig.straightRays) {
            float pixelCenter = width/2.0;
            if(screenConfig.perPixelRays && rayPositionX <= (float)width)//blue pixel
                pixelCenter = width*0.825;//<--center blue subpixel
            if(screenConfig.perPixelRays && rayPositionX < (float)width*0.66)//green pixel
                pixelCenter = width*0.5;//<--center of green subpixel
            if(screenConfig.perPixelRays && rayPositionX < (float)width*0.33)//red pixel
                pixelCenter = width*0.165;//0.33/2 <--center red subpixel

            float directionX = rayPositionX - pixelCenter;
            rayDirection = glm::normalize(glm::vec3(glm::vec3(0,height*2,0) +//the larger the value on Y, the shorter the span on X for the direction
                                                    glm::vec3(directionX, 0, 0)));
        }

        rays.emplace_back(new LineStrip(camera,
                                        shaderProgramId,
                                        Solver::GetInstance().CalculateRayTrajectory(lensConfig,
                                                                                     lensSheet,
                                                                                     rayStart,
                                                                                     rayDirection,
                                                                                     screenConfig.userDistanceFromScreen),
                                        rayColor));

                                        }
}

//this is redundant, gets called several times for each pixel, think on a way
//to destroy pixel only once, same for rays and lenses
void Pixel::DestroyPixel() {
    //destroy model
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &verticesBackgroundBufferHandle);
    glDeleteBuffers(1, &redSubPixelVerticesBufferHandle);
    glDeleteBuffers(1, &greenSubPixelVerticesBufferHandle);
    glDeleteBuffers(1, &blueSubPixelVerticesBufferHandle);

    Utils::ExitOnGLError("Couldnt destroy pixel vertex array");

    glDeleteVertexArrays(1, &pixelBackgroundVAO);
    glDeleteVertexArrays(1, &redSubPixelVAO);
    glDeleteVertexArrays(1, &greenSubPixelVAO);
    glDeleteVertexArrays(1, &blueSubPixelVAO);

    glBindVertexArray(0);
    Utils::ExitOnGLError("Couldnt destroy pixel VAO");
}

void Pixel::SetPixelColor(const glm::vec4 &newColor) {
    pixelColorArr[0] = newColor.r;
    pixelColorArr[1] = newColor.g;
    pixelColorArr[2] = newColor.b;
    pixelColorArr[3] = newColor.a;
}
