#version 400

layout(location=0) in vec3 position;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec4 color;

out vec4 ex_color;

void main(void) {
    gl_Position = projectionMatrix * viewMatrix * modelMatrix *
        vec4(position.x, position.y, position.z, 1.0);

    ex_color = color;
}
