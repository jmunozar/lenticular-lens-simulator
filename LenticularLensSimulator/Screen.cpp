#include "Screen.h"

using namespace::std;

Screen::Screen(Camera &_renderingCamera, const glm::vec3 &pos, GLuint _shaderProgramId,
               ScreenConfig &_screenConfig, LenticularSheet &_lensSheet) :
    position {pos}, screenConfig {_screenConfig}, renderingCamera {_renderingCamera}, shaderProgramId {_shaderProgramId},
    lensSheet{_lensSheet} {

    Initialize();

}

void Screen::Initialize() {
    pixels.clear();
    for(int i = 0; i < screenConfig.numberOfPixels; i++) {
        pixels.emplace_back(new Pixel(renderingCamera,
                                      position +glm::vec3((float)i*((float)screenConfig.pixelWidth+screenConfig.spaceBetweenPixels),0,0),
                                      screenConfig.pixelWidth,
                                      screenConfig.pixelHeight,
                                      screenConfig.raysPerPixel,
                                      lensSheet.GetLensConfig(),
                                      screenConfig,
                                      lensSheet,
                                      shaderProgramId,
                                      glm::vec4(0,0,0,1)));//black pixel as bg
    }
}

void Screen::Render() {
    //for(int i = 0; i < pixels.size(); i++)
    //    pixels[i]->Render();
}
