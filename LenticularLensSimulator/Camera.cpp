#include "Camera.h"

using namespace::std;

Camera::Camera(GLFWwindow* windowRef) : window {windowRef} {}

void Camera::GetInput(float scrollDelta) {
    int width;
    int height;
    glfwGetFramebufferSize(window, &width, &height);

    static double lastTime = glfwGetTime();
    double currentTime = glfwGetTime();

    float deltaTime = float(currentTime - lastTime);

    glm::vec3 direction = glm::vec3(0, 0, -1);//pointing front (towards screen)
    glm::vec3 right = glm::vec3(1, 0, 0);
    glm::vec3 up = glm::cross(right, direction);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        position += up * deltaTime * speed;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        position -= up * deltaTime * speed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        position += right * deltaTime * speed;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        position -= right * deltaTime * speed;

    size += scrollDelta * zoomSensitivity;
    //cout << "size: " << size << endl;
    projectionMatrix = glm::ortho(-size,
                                  size,
                                  -size,
                                  size,
                                  nearPlaneDistance,
                                  farPlaneDistance);
    viewMatrix       = glm::lookAt(position,           // Camera is here
                                   position+direction, // and looks here : at the same position, plus "direction"
                                   up);                // Head is up (set to 0,-1,0 to look upside-down)

    //cout << "Camera Pos: "  << glm::to_string(position) << " -- Direction: " << glm::to_string(direction) << " -- UP: " << glm::to_string(up) << " -- SIZE: " << size << endl;
    scrollDelta = 0;
    lastTime = currentTime;// For the next frame, the "last time" will be "now"
}
