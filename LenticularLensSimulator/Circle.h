#pragma once


#include <GL/glew.h>

#include <iostream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com
 */

class Circle {
 private:
    float radius;

    std::vector<glm::vec3> vertices;

    Camera& camera;//<--rendering camera
    glm::mat4 modelMatrix = glm::mat4(1.0f);
    float circleColor[4];

    //glsl handles
    GLuint projectionHandle;
    GLuint viewHandle;
    GLuint modelHandle;
    GLuint colorHandle;

    GLuint verticesBufferHandle;
    GLuint shaderProgramId;
    GLuint circleVAO;

    void LoadCircleShader();
    void InitializeCircleForRendering();

    void GenerateCircleVertices();

 public:
    Circle(Camera &renderingCamera, GLuint _shaderProgramId, const glm::vec3 &center, float radius, const glm::vec4 color);
    ~Circle();
    void Render();
    void SetPosition(const glm::vec3 &newPosition);
    glm::vec3 GetPosition();

};
