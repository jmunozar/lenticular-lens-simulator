#include "LenticularSheet.h"

using namespace::std;

LenticularSheet::LenticularSheet(Camera &_renderingCamera, const glm::vec3 &pos,
                                 const glm::vec4 &_sheetColor, GLuint _shaderProgramId,
                                 LenticularConfig &_lensConfig)
    : renderingCamera {_renderingCamera}, position {pos}, sheetColor {_sheetColor},
      shaderProgramId {_shaderProgramId}, lensConfig {_lensConfig} {


    Initialize();
    /* debug, lenses touch points
    for(int i = 0; i < lensConfig.lensesPerSheet-1; i++) {
        float Xc = lensCenters[i].x + lensConfig.lenticularPitch/2;
        Debug::GetInstance().DrawX(renderingCamera, _shaderProgramId, glm::vec3(Xc, lensConfig.lenticularSubstrateThickness, 0), glm::vec4(0,1,0,1));
        //cout << "Calculated lens pitch: " << lensCenters[i+1].x-lensCenters[i].x << endl;
    }
    */

}

void LenticularSheet::Initialize() {
    lensCenters.clear();
    lenses.clear();


    float Yc = std::sqrt(std::pow(lensConfig.lenticularRadius, 2.0) -
                         std::pow(lensConfig.lenticularPitch/2.0, 2.0));//point where lenses touch on the Y axis

    float lensAngle = Utils::Rad2Deg(2*acos(Yc/lensConfig.lenticularRadius));//aperture angle of each lens

    for(int i = 0; i < lensConfig.lensesPerSheet; i++) {
        glm::vec3 lensCenter = position + glm::vec3(lensConfig.lenticularPitch/2 + ((float)i)*lensConfig.lenticularPitch,
                                               lensConfig.lenticularSubstrateThickness-Yc,//move center down
                                               0);
        //emplace, creates directly to mem without copy ctor
        lenses.emplace_back(new Lens(renderingCamera,
                                     lensCenter,
                                     sheetColor,
                                     lensConfig.lenticularRadius,
                                     lensAngle,
                                     shaderProgramId));
        lensCenters.push_back(lensCenter);
        //Debug::GetInstance().DrawX(lensCenters[lensCenters.size()-1], glm::vec4(1,1,0,1));
    }

    //bounday drawing
    vector<glm::vec3> boundaryVertices;
    boundaryVertices.push_back(position + glm::vec3(0, lensConfig.lenticularSubstrateThickness, 0));
    boundaryVertices.push_back(position);
    boundaryVertices.push_back(position + glm::vec3(lensConfig.lenticularPitch*lensConfig.lensesPerSheet,0,0));
    boundaryVertices.push_back(position + glm::vec3(lensConfig.lenticularPitch*lensConfig.lensesPerSheet, lensConfig.lenticularSubstrateThickness,0));
    lenticularSheetBoundaries.reset(new LineStrip(renderingCamera, shaderProgramId, boundaryVertices, sheetColor));
    /****************/
}

void LenticularSheet::Render() {
    for(int i = 0; i < lenses.size(); i++) {
        lenses[i]->Render();
    }
    lenticularSheetBoundaries->Render();
}


