#version 400

layout(location = 0) in vec2 vertexPosScreenSpace;
layout(location = 1) in vec2 vertexUV;

out vec2 UV;

void main() {
    //map[0..800][0..600] -> [-1..1][-1..1]
    vec2 vertexPosHomogeneousSpace = (vertexPosScreenSpace - vec2(400, 300));
    vertexPosHomogeneousSpace /= vec2(400,300);

    gl_Position = vec4(vertexPosHomogeneousSpace, 0, 1);

    UV = vertexUV;
}
