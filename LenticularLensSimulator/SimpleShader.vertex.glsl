#version 400

layout(location=0) in vec3 vertexPosModelSpace;
layout(location=1) in vec2 inUV;
layout(location=2) in vec3 inNormal;


out vec2 vertexUV;
out vec3 vertexPosWorldSpace;
out vec3 normalCamSpace;
out vec3 eyeDirCamSpace;
out vec3 lightDirCamSpace;

//constant values for the mesh
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform vec3 lightPosWorldSpace;


void main(void) {
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosModelSpace,1);

    vertexPosWorldSpace = (modelMatrix * vec4(vertexPosModelSpace,1)).xyz;

    //vector that goes from the vertex to the camera. (in cam space, the cam is at 0,0,0)
    vec3 vertexPosCamSpace = (viewMatrix * modelMatrix * vec4(vertexPosModelSpace, 1)).xyz;
    eyeDirCamSpace = vec3(0,0,0) - vertexPosCamSpace;

    //vector that goes from the vertex to the light, in cam space modelMatrix is ommited bc is identity
    vec3 lightPosCamSpace = (viewMatrix * vec4(lightPosWorldSpace, 1)).xyz;
    lightDirCamSpace = lightPosCamSpace + eyeDirCamSpace;

    //normal of the vertex in cam space
    normalCamSpace = (viewMatrix * modelMatrix * vec4(inNormal, 0/*0 bc is a dir*/)).xyz;//only correct if modelMatrix is not scaled, use its inverse transpose if not

    vertexUV = inUV;
}
