#ifndef LENTICULARSHEET_H
#define LENTICULARSHEET_H

#include <iostream>
#include <memory>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"
#include "ConfigStructures.h"
#include "Lens.h"
#include "LineStrip.h"
#include "Debug.h"
/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This class represents a set of lenses that refract rays from the pixels
 */
class LenticularSheet {
 private:
    Camera& renderingCamera;
    glm::vec3 position;
    glm::vec4 sheetColor;

    std::vector<glm::vec3> lensCenters;
    std::vector<std::unique_ptr<Lens>> lenses;

    GLuint shaderProgramId;

    std::unique_ptr<LineStrip> lenticularSheetBoundaries;

    LenticularConfig& lensConfig;
 public:
    LenticularSheet(Camera &_renderingCamera, const glm::vec3 &pos, const glm::vec4 &sheetColor, GLuint _shaderProgramId, LenticularConfig &_lensCfg);
    //~LenticularSheet(); no need

    const std::vector<glm::vec3>& GetLensCenters() const { return lensCenters; } //const at the end: reading of a class variables is ok inside of the function, but writing inside of this function will generate a compiler error.
    const glm::vec3& GetPosition() const { return position; }

    const LenticularConfig& GetLensConfig() { return lensConfig; }

    void Render();
    void Initialize();
};
#endif
