#pragma once

#include <GL/glew.h>
//#include <vector>

#include <array>

#include <glm/glm.hpp>
#include "Camera.h"

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  Simple class for drawing squares
 */

class Square {
 private:
    Camera &camera;
    glm::mat4 modelMatrix;
    float squareColor[4];

    float squareWidth;
    float squareHeight;

    std::array<glm::vec3, 4> vertices;

    //glsl values
    GLuint projectionHandle;
    GLuint viewHandle;
    GLuint modelHandle;
    GLuint colorHandle;

    GLuint verticesBufferHandle;
    GLuint shaderProgramId;
    GLuint squareVAO;

    void LoadSquareShader();
    void InitializeSquareForRendering();
    void GenerateSquareVertices();

 public:
    Square(Camera &renderingCamera, GLuint _shaderProgramId, const glm::vec3 &position,
           float width, float height, const glm::vec4 &color);
    ~Square();
    void Render();
};
