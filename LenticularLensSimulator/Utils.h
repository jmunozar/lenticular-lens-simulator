#pragma once

#include <cmath>
#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <cstdlib>

#include <vector>
#include <stdio.h>

#include "ConfigStructures.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

using namespace::std;

class Utils {
 private:
    static void LTrim(std::string &s);
    static void RTrim(std::string &s);
    static void Trim(std::string &s);
    static vector<string> SplitToStringVec(const string& s, char delimiter);
    static vector<int> ParseToIntArray(const string &value);
    static vector<float> ParseToFloatArray(const string &value);
    static bool StringIsArray(const std::string &value);
    static bool StringIsFloat(const std::string &value);
    static bool StringIsBool(const std::string &value);

 public:
    static double Cotangent(double angle) { return (double)(1.0 / tan(angle)); };
    static double Deg2Rad(double deg) { return deg * (glm::pi<double>() / 180); };
    static double Rad2Deg(double rad) { return rad * (180 /glm::pi<double>()); };

    static GLuint LoadSpecificShader(const std::string &fileName, GLenum shaderType);
    static GLuint LoadAndLinkShadersToProgram(const std::string &vertexShaderFileName, const std::string &fragmentShaderFileName);
    static void ExitOnGLError(const std::string &errorMsg);

    static GLuint LoadDDS(const std::string &fileName);

    //returns true if file could be parsed correctly
    static bool LoadConfigFile(const std::string &path, LenticularConfig &refLensConfig, ScreenConfig &refScreenCfg);
    static glm::vec4 GenerateRandomColor(float alpha = 1);

    static float RoundFloat(float val);
};
