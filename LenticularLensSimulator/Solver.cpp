#include "Solver.h"

using namespace::std;

vector<glm::vec3> Solver::CalculateRayTrajectory(const LenticularConfig &lensConfig, const LenticularSheet &lensSheet,
                                                 const glm::vec3 &rayPos, const glm::vec3 &rayDir, float userDistanceFromScreen) {
    vector<glm::vec3> points;
    points.push_back(rayPos);
    //Debug::GetInstance().DrawX(rayPos, glm::vec4(1,1,0,1)/*green*/);//cross where the ray starts
    //draw intersection line where the substrate ends
    glm::vec3 p1l1 = lensSheet.GetPosition() + glm::vec3(-1,lensConfig.lenticularSubstrateThickness,0);
    glm::vec3 p2l1 = lensSheet.GetPosition() + glm::vec3(1 + lensConfig.lenticularPitch*lensConfig.lensesPerSheet,
                                                         lensConfig.lenticularSubstrateThickness,
                                                         0);
    //Debug::GetInstance().DrawLine(p1l1, p2l1, glm::vec4(1,0,0,1)/*RED Color*/);//line where substrate thickness intersect

    Line l1 = Line(glm::vec2(p1l1), glm::vec2(p2l1));//substrate line
    Line l2 = Line(glm::vec2(rayPos), glm::vec2(rayPos + rayDir));//ray line


    glm::vec2 linesPointIntersection;//point of intersection with the substrate
    //find where the rays intersect with the substrate
    if(LinesCross(l1, l2, linesPointIntersection)) {
        //Debug::GetInstance().DrawX(glm::vec3(linesPointIntersection.x, linesPointIntersection.y, 0), glm::vec4(0,1,0,1)/*green*/);
        //OJOOOOOO !!!!!!!! points.push_back(glm::vec3(linesPointIntersection.x, linesPointIntersection.y, 0)); <--ESTO ESTABA DESCOMENTADO--NoNeedForThisPoint.PEEERO:SiAgregamosEstePunto__Y___UsamosLINESEnVezDeLINE_STRIPEnLineStrip.cpp_PodriamosGenerarMasPerformance.

        //find the closest center with the line intersection with the substrate
        float minimumDistanceToLensCenter = numeric_limits<float>::max();
        int closestCenterIndex = -1;
        for (int i = 0; i < lensSheet.GetLensCenters().size(); i++) {
            float distanceToLensCenter = glm::distance(glm::vec2(lensSheet.GetLensCenters()[i]), linesPointIntersection);
            if(minimumDistanceToLensCenter > distanceToLensCenter) {
                minimumDistanceToLensCenter = distanceToLensCenter;
                closestCenterIndex = i;
            }
        }

        //find point where ray intersects with lens that has the closest center
        glm::vec2 lineLensIntersection;
        if(LineLensIntersect(l2, glm::vec2(lensSheet.GetLensCenters()[closestCenterIndex]),
                             lensConfig.lenticularRadius, lineLensIntersection)) {


            //cout << "Line Lens Intersect: " << lineLensIntersection.x << ", " << lineLensIntersection.y << endl;
            points.push_back(glm::vec3(lineLensIntersection.x, lineLensIntersection.y, 0));//point that intersects ray with lens
            //after having the intersection with the lens, lets calculate the refraction
            glm::vec2 refractedPoint2D = lineLensIntersection + GetRefractedEndPoint(glm::vec2(rayPos),
                                                                                     lineLensIntersection,
                                                                                     glm::vec2(lensSheet.GetLensCenters()[closestCenterIndex]),
                                                                                     lensConfig.lenticularLensRefraction);

            //only draw if the refraction is valid
            if(refractedPoint2D != glm::vec2(std::numeric_limits<float>::max(), std::numeric_limits<float>::max())) {
                Line refractedLine = Line(lineLensIntersection, refractedPoint2D);
                points.push_back(glm::vec3(refractedLine.GetX(userDistanceFromScreen), userDistanceFromScreen, 0));
            }
        } else {
            //cerr << "ERROR: Ray doesnt intersect with lens" << endl;
        }
    } else {
        Debug::GetInstance().DrawX(rayPos, glm::vec4(1,0,0,1));
        cerr << "ERROR: Ray doesnt touch lenticular lens substrate" << endl;
    }

    return points;
}

/*Line Solver::GetTangentFromCircleAtPoint(const glm::vec2 &pointToGetTangent, const glm::vec2 &lensCenter) {
    Line lineEquationLensCenterToTangentPoint(pointToGetTangent, lensCenter);
    double tangentLineM = -lineEquationLensCenterToTangentPoint.GetM();
    double tangentLineB = (-(1/tangentLineM)*pointToGetTangent.x) + pointToGetTangent.y;

    Line tangentLine = Line(1/tangentLineM, tangentLineB);
    return tangentLine;
}*/

//Adapted from: https://www.flipcode.com/archives/reflection_transmission.pdf
glm::vec2 Solver::GetRefractedEndPoint(const glm::vec2 &rayOriginPos, const glm::vec2 &rayLensIntersection, const glm::vec2 &lensCenter, double lensRefractionIndex) {
    glm::vec2 normal = glm::normalize(lensCenter - rayLensIntersection);// - lensCenter);
    glm::vec2 incident = glm::normalize(rayLensIntersection - rayOriginPos);// - rayOriginPos);

    double n = lensRefractionIndex / 1.00;//1.00 vacum refraction
    double cosI = glm::dot(normal, incident);
    double sinT2 = std::pow(n, 2) * (1.0 - std::pow(cosI, 2));
    if(sinT2 > 1) {
        //cerr << "Invalid refraction" << endl;
        return glm::vec2(std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
    }
    return (float)n * incident - ((float)n + (float)sqrt(1.0-sinT2))*normal;
}

//Adapted from: http://apetrilla.blogspot.com/2011/12/circle-and-line-intersection.html
bool Solver::LineLensIntersect(const Line &line, const glm::vec2 &lensCenter, float radius, glm::vec2 &intersectionPoint) {
    const glm::vec2 invalidIntersection = glm::vec2(std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    float xIntersect;
    float yIntersect;

    float solution = 1;//this represents the solution we need depending on
                       //the slope of the line that intersects, if its neg
                       //(Meaning the line is like this "\", we
                       //get the negative solution, else we get the positive
                       //solution (when theline is like this "/")

    if(!line.HasInfiniteSlope()) {
        //                           - b²            +           r²        +                      m²r²                    -                2bmu
        float B2Minus4AC = -std::pow(line.GetB(), 2) + std::pow(radius, 2) + (std::pow(line.GetM(),2)*std::pow(radius,2)) - (2*line.GetB()*line.GetM()*lensCenter.x)
        //                          - m²u²                         +                2bv               +                   2muv                    -            v²
            -(std::pow(line.GetM(), 2) * std::pow(lensCenter.x,2)) + (2 * line.GetB() * lensCenter.y) + (2*line.GetM()*lensCenter.x*lensCenter.y) - std::pow(lensCenter.y, 2);

        if(B2Minus4AC < 0) {
            intersectionPoint = invalidIntersection;
            return false;
        } else {// o or > 0 intersects and we are only interested in the positive value
            if(!line.HasInfiniteSlope() && line.GetM() < 0) {
                solution = -1;
            }
            //           (-bm + u + mv + sqrt(B2Minus4AC) ) / (1 + m2)
            xIntersect = (((-(line.GetB()*line.GetM()) + lensCenter.x + (line.GetM()*lensCenter.y) + (solution * std::sqrt(B2Minus4AC))) / (1 + std::pow(line.GetM(), 2))) - lensCenter.x);
            yIntersect = (line.GetY(xIntersect+lensCenter.x) - lensCenter.y);
        }
    } else {
        xIntersect = line.GetFunctionIfInfiniteSlope() - lensCenter.x;
        yIntersect = std::sqrt(std::pow(radius,2) - std::pow(xIntersect, 2));
    }
    intersectionPoint = lensCenter + glm::vec2(xIntersect, yIntersect);
    return true;
}

bool Solver::LinesCross(const Line &l1, const Line &l2, glm::vec2 &crossPoint) {
    const glm::vec2 invalidCrossPoint = glm::vec2(std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    float xIntersect = -99;
    float yIntersect = -99;

    if(l1.HasInfiniteSlope() && l2.HasInfiniteSlope()) {//both lines are vertical and are in the same place
        if(l1.GetFunctionIfInfiniteSlope() == l2.GetFunctionIfInfiniteSlope()) {
            crossPoint = glm::vec2(l1.GetFunctionIfInfiniteSlope(), 0);
            return true;
        } else {
            crossPoint = invalidCrossPoint;
            return false;
        }
    } else if(l1.HasInfiniteSlope() && !l2.HasInfiniteSlope()) {//l1 infinite slope, l2 no infiniteSlope
        xIntersect = l1.GetFunctionIfInfiniteSlope();
        yIntersect = l2.GetY(xIntersect);
    } else if(!l1.HasInfiniteSlope() && l2.HasInfiniteSlope()) {//l1 NO inf slope, l1 inf Slope
        xIntersect = l2.GetFunctionIfInfiniteSlope();
        yIntersect = l1.GetY(xIntersect);
    } else if(!l1.HasInfiniteSlope() && !l2.HasInfiniteSlope()) {// l1 & l2 no inf slope
        if(l1.GetM() == l2.GetM()) {
            crossPoint = invalidCrossPoint;
            return false;
        } else {
            xIntersect = (l2.GetB() - l1.GetB()) / (l1.GetM() - l2.GetM());
            yIntersect = l1.GetY(xIntersect);
        }
    }
    //cout << "Intersection: " << crossPoint.x << ", " << crossPoint.y << endl;
    if(xIntersect == -99 && yIntersect == -99)
        cout << "ERROR: intersect with x and y where not initialized. xIntersect: " << xIntersect << " yIntersect: " << yIntersect << endl;
    crossPoint = glm::vec2(xIntersect, yIntersect);
    return true;
}
