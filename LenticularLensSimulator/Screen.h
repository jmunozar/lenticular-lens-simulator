#pragma once

#include <iostream>
#include <memory>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include "Camera.h"
#include "Pixel.h"
#include "LenticularSheet.h"
#include "ConfigStructures.h"

/*
  Created by:
  Juan Sebastian Munoz Arango
  naruse@gmail.com

  This is the screen class represents a set of pixels (vector of unique_ptr).
  each pixel has a # of rays
 */
class Screen {
 private:
    glm::vec3 position;
    std::vector<std::unique_ptr<Pixel>> pixels;
    ScreenConfig& screenConfig;
    Camera& renderingCamera;
    GLuint shaderProgramId;
    LenticularSheet& lensSheet;

 public:
    Screen(Camera &cam, const glm::vec3 &position, GLuint pixelShaderProgramId,
           ScreenConfig &screenCfg, LenticularSheet &_lensSheet);
    //    ~Screen();
    void Render();

    const ScreenConfig& GetScreenConfig() { return screenConfig; }
    const std::vector<std::unique_ptr<Pixel>>& GetScreenPixels() { return pixels; }

    void Initialize();

    void SetRaysPerPixel(int raysPP) { screenConfig.raysPerPixel = raysPP; }
    void SetRaysTransparency(float t) { screenConfig.raysTransparency = t; }
    void SetPerPixelRays(int ppRays) { screenConfig.perPixelRays = ppRays; }
    void SetStraightRays(bool _straightRays) { screenConfig.straightRays = _straightRays; }
    void ResetPixelColors() { for(int i = 0; i < pixels.size(); i++) pixels[i]->SetPixelColor(glm::vec4(0,0,0,1)); }
    void RenderPixelRays(bool val)  { for(int i = 0; i < pixels.size(); i++) pixels[i]->RenderPixelRays(val); }
};
